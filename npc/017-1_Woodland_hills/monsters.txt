
017-1,0,0,0,0	monster	Bat	1017,5,0,0,Mob33::OnBat
017-1,0,0,0,0	monster	FireGoblin	1011,5,0,0,Mob33::OnFireGoblin
017-1,0,0,0,0	monster	Mouboo	1028,5,0,0,Mob33::OnMouboo
017-1,0,0,0,0	monster	Flower	1014,5,0,0,Mob33::OnFlower
017-1,0,0,0,0	monster	SpikyMushroom	1019,5,0,0,Mob33::OnSpikyMushroom
017-1,0,0,0,0	monster	EvilMushshroom	1013,10,0,0,Mob33::OnEvilMushroom

017-1,0,0,0,0	monster	Alizarin	1032,3,0,0,Mob33::OnAlizarin
017-1,0,0,0,0	monster	Gamboge	1031,3,0,0,Mob33::OnGamboge
017-1,0,0,0,0	monster	Cobalt	1030,3,0,0,Mob33::OnCobalt
017-1,0,0,0,0	monster	Mauve	1029,5,0,0,Mob33::OnMauve

017-1,0,0,0,0	monster	SilkWorm	1035,10,0,0,Mob33::OnSilkWorm

017-1,0,0,0,0	monster	Squirrel	1038,30,20,10,Mob33::OnSquirrel

017-1,0,0,0	script	Mob33	NPCMINUS1,{
OnBat:
	set @mobId, 1017;
	callfunc "MobPoints";
	end;

OnFireGoblin:
	set @mobId, 1011;
	callfunc "MobPoints";
	end;

OnMouboo:
	set @mobId, 1028;
	callfunc "MobPoints";
	end;

OnFlower:
	set @mobId, 1014;
	callfunc "MobPoints";
	end;

OnSpikyMushroom:
	set @mobId, 1019;
	callfunc "MobPoints";
	end;

OnEvilMushroom:
	set @mobId, 1013;
	callfunc "MobPoints";
	end;

OnAlizarin:
	set @mobId, 1032;
	callfunc "MobPoints";
	end;

OnGamboge:
	set @mobId, 1031;
	callfunc "MobPoints";
	end;

OnCobalt:
	set @mobId, 1030;
	callfunc "MobPoints";
	end;

OnMauve:
	set @mobId, 1029;
	callfunc "MobPoints";
	end;

OnSilkWorm:
	set @mobId, 1035;
	callfunc "MobPoints";
	end;

OnSquirrel:
	set @mobId, 1038;
	callfunc "MobPoints";
	end;
}
