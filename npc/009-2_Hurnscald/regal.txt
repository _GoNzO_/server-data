
009-2,119,44,0	script	Regal	NPC127,{
	mes "Du siehst ein Regal mit sehr alten Büchern";
	next;
	menu 
			"Durchsuchen.", L_durchsuchen,
			"Weggehen.", -;
			close;
			
	L_durchsuchen:
	if (QUEST_Silvia_Buch == 1) goto L_success;
	if (QUEST_Silvia_Buch == 0) goto L_nichts;
	
	L_success:
	mes "Unter den ganzen Schmökern findest du das Tagebuch von Katrin Tripson";
	mes "Du legst es in einen speziellen Platz im Inventar.";
	set QUEST_Silvia_Buch, 2;
	close;
	
	L_nichts:
	mes "Du findest nichts von Interesse";
	close;
}