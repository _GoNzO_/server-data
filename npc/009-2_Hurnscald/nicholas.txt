
009-2,185,55,0	script	Nicholas	NPC135,{

        @SETZER_XP = 100000;
        @SHIELD_XP = 20000;

        @SHIELD_INITIAL = 0;
        set @SHIELD_KNOWS_PATCH, 1;	// Spieler weiß dass ein Leder Stück gebraucht wird
        set @SHIELD_COMPLETED, 2;	// Schild ist einmal komplett angefertigt

        @SETZER_INITIAL = 0;
        @SETZER_KNOWS_OIL = 1;
        @SETZER_KNOWS_STINGER = 2;
        @SETZER_FLAG_MADE_OIL = 4;
        @SETZER_FLAG_MADE_SETZER = 8;

        @Q_SETZER_MASK = NIBBLE_3_MASK;
        @Q_SETZER_SHIFT = NIBBLE_3_SHIFT;

        @Q_SHIELD_MASK = NIBBLE_4_MASK;
        @Q_SHIELD_SHIFT = NIBBLE_4_SHIFT;

        @Q_SETZER_status = (QUEST_Forestbow_state & @Q_SETZER_MASK) >> @Q_SETZER_SHIFT;
        @Q_SHIELD_status = (QUEST_Forestbow_state & @Q_SHIELD_MASK) >> @Q_SHIELD_SHIFT;

	mes "[Nicholas]";
	mes "\"Hallo,";
	mes "ich bin ein Meisterschmied.";
	mes "Wenn du mir etwas Eisenerz bringst";
	mes "könnte ich Dir ein sehr nützliches Schild";
	mes "herstellen, oder einen Helm.\"";
	next;

	if (@Q_SETZER_status & @SETZER_FLAG_MADE_SETZER) goto L_Menu;
	menu
		"Ich habe etwas Eisenerz!", L_Check,
		"Wo kann ich dieses Eisenerz bekommen?", L_Info,
		"Alles klar, danke.", L_Pass;
L_Menu:
	menu
		"Ich habe von einer Legende über eine Waffe gehört...", L_Story,
		"Ich habe etwas Eisenerz!", L_Check,
		"Wo kann ich dieses Eisenerz bekommen?", L_Info,
		"Alles klar, danke.", L_Pass;


L_Story:

	if (QUEST_rock_knife==2) goto L_Traenke;
	if (QUEST_rock_knife>2) goto L_Info1;

	mes "[Nicholas]";
	mes "\"Das ist schon sehr lange her. Ein junger";
	mes "Krieger fand damals einen seltsamen";
	mes "handgroßen Stein, dieser war härter als alle";
	mes "anderen Materialien die man damals kannte.\"";
	next;
	mes "\"Mein Großvater besaß die Gabe aus diesem";
	mes "Gestein eine Klinge zu schmieden. Viel mehr";
	mes "kann ich Dir im Moment nicht erzählen.\"";
	next;
	mes "Nicholas macht einen müden Eindruck und setzt";
	mes "sich schweigend in eine Ecke.";
	if (countitem(687)<1) goto L_noPotion;
	menu
		"Du siehst erschöpft aus, hier nimm diesen Trank.",L_Potion,
		"Danke, ich suche dann weiter",-;
	close;

L_Check:
	mes "[Nicholas]";
	mes "\"Zeig mir doch mal wieviel Du hast...\"";
	next;
	if(countitem("IronOre")<5) goto L_NoItem;
	if(countitem("IronOre")<10) goto L_StageA;
	if(countitem("IronOre")<15) goto L_StageB;
	goto L_StageC;

L_Info:
	mes "[Nicholas]";
	mes "\"Man kann Eisenerz in Minen finden. Es ist";
	mes "allerdings ziemlich schwer es zu bekommen.\"";
	close;

L_Pass:
	mes "[Nicholas]";
	mes "\"Oh, okay";
	mes "Du kannst jederzeit wieder kommen.\"";
	close;

L_Traenke:
	if (countitem(687)<5) goto L_nichtGenug;
	delitem 687, 5;
	set QUEST_rock_knife, 3;
	mes "[Nicholas]";
	mes "\"Ich danke Dir, zur Belohnung bekommst";
	mes "Du einen Hinweis wo Du weitersuchen kannst.\"";
	next;

L_Info1:
	mes "[Nicholas]";
	mes "\"Ich hörte dass der Held, der das Gestein";
	mes "damals endeckt hat, auf dem Friedhof begraben";
	mes "wurde und immer noch dort herum spukt. Er";
	mes "kann Dir sicher mehr dazu sagen, aber pass";
	mes "auf, dort ist es sehr gefährlich!\"";
	close;

L_noPotion:
	menu
		"Danke, ich suche dann weiter",-;
	close;

L_Potion:
	mes "Misstrauisch betrachtet Nicholas die kleine";
	mes "Flasche, deren Inhalt rosa schimmert. Doch";
	mes "schließlich trinkt er sie restlos aus.";
	delitem 687, 1;
	next;
	mes "[Nicholas]";
	mes "\"Ah das ist wundervoll, ich fühle mich wie";
	mes "ausgewechselt. Danke, kannst Du mir noch 5";
	mes "von diesem Wundergesöff bringen? Ich werde";
	mes "mich auch dafür revanchieren.";
	menu
		"Ok, ich schaue was sich machen lässt.", L_Potions,
		"Vergiss es.",-;
	close;

L_StageA:
	mes "[Nicholas]";
	mes "\"Das ist gerade genug dass ich Dir einen";
	mes "geflügelten Ritter Helm machen kann.";
	mes "Aber das wird Dich 10,000€ kosten";
	mes "und 5 Stücke Eisenerz.\"";
	next;
        goto L_Main_menu;

L_StageB:
	mes "[Nicholas]";
	mes "\"Ahh, der Menge Eisenerz kann ich Dir";
	mes "einen von zwei verschiedenen Helmen machen,";
	mes "für nur 10,000€, oder Ich kann Dir";
	mes "ein Schild für 20,000€ machen.\"";
	mes "";
	mes "\"Was möchtest du haben?\"";
	next;
        goto L_Main_menu;

L_StageC:
	mes "[Nicholas]";
	mes "\"Ausgezeichnet, das ist genug um drei";
	mes "verschiedene Helmtypen zu machen.";
	mes "Als Lohn bekomme ich 10,000€.";
        mes "Ich kann Dir auch ein Schild machen, aber das";
        mes "wird Dich 20,000€ kosten.\"";
	mes "";
	mes "\"Was darf es sein?\"";
	next;
        goto L_Main_menu;

L_nichtGenug:
	mes "[Nicholas]";
	mes "\"Wie ich sehe hast Du die Tränke noch nicht";
	mes "zusammen\"";
	close;

L_Potions:
	set QUEST_rock_knife, 2;
	close;

L_Main_menu:

        @CHOICE_KNIGHT = 1;
        @CHOICE_CRUSADE = 2;
        @CHOICE_WARLORD = 3;
        @CHOICE_SHIELD = 4;
        @CHOICE_SETZER = 5;
        @CHOICE_CANCEL = 6;

        setarray @choice$, "", "", "", "", "", "";
        @choices_nr = 0;
        setarray @choice_idx, 0, 0, 0, 0, 0, 0;

	if (countitem("ShortSword") < 1) goto L_Main_menu_post_setzer;
	set @choice$[@choices_nr], "Kannst du mein Kurzschwert aufrüsten?";
	set @choice_idx[@choices_nr], @CHOICE_SETZER;
	set @choices_nr, 1 + @choices_nr;

L_Main_menu_post_setzer:

	if (countitem("IronOre")<5) goto L_no_more_helmets;
        @choice$[@choices_nr] = "Knight's Helmet. (5 Iron Ores)";
        @choice_idx[@choices_nr] = @CHOICE_KNIGHT;
        @choices_nr = 1 + @choices_nr;

	if (countitem("IronOre")<10) goto L_no_more_helmets;

        @choice$[@choices_nr] = "Crusader Helm. (10 Iron Ores)";
        @choice_idx[@choices_nr] = @CHOICE_CRUSADE;
        @choices_nr = 1 + @choices_nr;

        @choice$[@choices_nr] = "Stahlschild. (10 Iron Ores)";
        @choice_idx[@choices_nr] = @CHOICE_SHIELD;
        @choices_nr = 1 + @choices_nr;

	if (countitem("IronOre")<15) goto L_no_more_helmets;

        @choice$[@choices_nr] = "Warlord Helm. (15 Iron Ores)";
        @choice_idx[@choices_nr] = @CHOICE_WARLORD;
        @choices_nr = 1 + @choices_nr;
L_no_more_helmets:
		set @choice$[@choices_nr], "Nichts, danke.";
		set @choice_idx[@choices_nr], @CHOICE_CANCEL;
		set @choices_nr, 1 + @choices_nr;

	menu	@choice$[0], -,
		@choice$[1], -,
		@choice$[2], -,
		@choice$[3], -,
		@choice$[4], -,
		@choice$[5], -;

        @menu = @menu - 1;

        if (@menu >= @choices_nr)
		close;

	set @choice, @choice_idx[@menu];

	if (@choice == @CHOICE_KNIGHT) goto L_YesKnight;
	if (@choice == @CHOICE_CRUSADE) goto L_YesCrusade;
	if (@choice == @CHOICE_WARLORD) goto L_YesWarlord;
	if (@choice == @CHOICE_SHIELD) goto L_YesShield;
	if (@choice == @CHOICE_SETZER) goto L_SetzerQuest;
	if (@choice == @CHOICE_CANCEL) goto L_End;

	close;                


L_NoItem:
	mes "[Nicholas]";
	mes "\"Es scheint so als ob Du nicht genug";
	mes "Eisenerz hast aus dem ich etwas machen könnte.";
	mes "Aber bitte komm' zurück wenn Du mehr hast.\"";
	close;

L_ComeBack:
	mes "[Nicholas]";
	mes "\"Du kannst jederzeit wieder kommen.\"";
	close;

L_NoMoney:
	mes "[Nicholas]";
	mes "\"Oje, es sieht so aus als ob Du nicht";
	mes "genügend Geld hast.\"";
	close;

L_YesKnight:
	if (Zeny < 10000) goto L_NoMoney;
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	set Zeny, Zeny - 10000;
	delitem "IronOre", 5;
	getitem "KnightsHelmet", 1;
	goto L_Done;

L_YesCrusade:
	if (Zeny < 10000) goto L_NoMoney;
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	set Zeny, Zeny-10000;
	delitem "IronOre", 10;
	getitem "CrusadeHelmet", 1;
	goto L_Done;

L_YesWarlord:
	if (Zeny < 10000) goto L_NoMoney;
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	set Zeny, Zeny-10000;
	delitem "IronOre", 15;
	getitem "WarlordHelmet", 1;
	goto L_Done;

L_YesShield:
        mes "Nicholas inspiziert Dein Eisenerz.";
	mes "";
        mes "[Nicholas]";
        mes "\"Nein, dieses Eisen ist zu spröde; für etwas";
	mes "so großes wie ein Schild muss ich weicheres";
	mes "Eisen beimischen. Kann ich mal sehen ob Du";
	mes "etwas brauchbares hast?\"";
        next;

        menu	"Sicher, sieh Dir alles an!", -,
		"Nein.", L_Pass;

	if (countitem("InfantryHelmet") == 1) goto L_MoreInfantry;
        if (countitem("InfantryHelmet") == 0) goto L_NoInfantry;

        mes "[Nicholas]";
        mes "Nicholas holt zwei von Deinen Infanterie";
	mes "Helmen heraus.";
        mes "\"Ich könnte die benutzen... ja, das sollte";
	mes "gehen. Jetzt ist alles was ich noch brauche";
	mes "ein Stück Leder für den Griff, und 20,000 €.\"";

        if (@Q_SHIELD_status < @SHIELD_KNOWS_PATCH)
        	set @Q_SHIELD_status, @SHIELD_KNOWS_PATCH;
        callsub L_Update_Var;

        next;

	menu	"Hier, bitteschön.", -,
		"Wo kann ich ein Stück Leder bekommen?", L_WhereLeather,
        	"Auf keinen Fall.", L_Pass;

	if (Zeny < 20000) goto L_ShieldNoZeny;
	if (countitem("InfantryHelmet") < 2) goto L_ShieldNoInfantry;
	if (countitem("IronOre") < 10) goto L_ShieldNoOre;
	if (countitem("LeatherPatch") < 1) goto L_ShieldNoLeatherPatch;
	// Kein inventory check benötigt, da Infantry Helme entfernt werden, werden zwei slots frei

        mes "[Nicholas]";
        mes "\"Ja, sieht aus als ob du alles hast";
	mes "was ich brauche!\"";
        mes "Du siehst Nicholas zu wie er das Erz und die";
	mes "Helme schmilzt und ein Schild aus dem sich";
	mes "bildenden Eisen formt. Dann schneidet er das";
	mes "Stück Leder auseinander und bringt es am";
	mes "Griff an.";
        mes "\"Hier ist Dein Schild!\"";

        delitem "InfantryHelmet", 1;
        delitem "InfantryHelmet", 1;
        delitem "LeatherPatch", 1;
        delitem "IronOre", 10;
        Zeny = Zeny - 20000;

        if (@Q_SHIELD_status < @SHIELD_COMPLETED)
		getexp @SHIELD_XP, 0;
        if (@Q_SHIELD_status < @SHIELD_COMPLETED)
		mes "[" + @SHIELD_XP + " experience points]";

        set @Q_SHIELD_status, @SHIELD_COMPLETED; // get XP only once
        callsub L_Update_Var;

        getitem "SteelShield", 1;
        close;

L_ShieldNoZeny:
        mes "[Nicholas]";
        mes "\"Tut mir leid, aber Ich muss Dich bitten";
	mes "20,000 € zu zahlen; das ist Qualitätsarbeit.\"";
        close;

L_ShieldNoInfantry:
        mes "[Nicholas]";
        mes "\"Das ist komisch ... Ich könnte schwören dass";
	mes "Du gerade noch zwei Infanterie Helme hier";
	mes "gehabt hast. Nun, komm' zurück wenn Du ein";
	mes "paar mehr hast!\"";
        close;

L_ShieldNoOre:
        mes "[Nicholas]";
        mes "\"Wie seltsam ... habe ich nicht Dein Eisenerz";
	mes "hier auf den Tisch gelegt? Nun, Ich werde es";
	mes "brauchen um das Schild zu machen.\"";
        close;

L_ShieldNoLeatherPatch:
        mes "[Nicholas]";
        mes "\"Du hast kein passendes Stück Leder. Tut mir";
	mes "leid, aber ein Schild ohne würde schrecklich";
	mes "scheuern.\"";
        close;

L_MoreInfantry:
        mes "[Nicholas]";
        mes "Nicholas nimmt Deinen Infanterie Helm und";
	mes "betrachtet ihn.";
        mes "\"Ja, der ist perfekt! Wenn Du mir noch so";
	mes "einen bringst, Kann ich Dir das Schild";
	mes "herstellen.\"";
        close;
	
L_NoInfantry:
        mes "[Nicholas]";
        mes "Nicholas schüttelt den Kopf.";
        mes "\"Nein, nichts davon ist brauchbar. Versuche";
	mes "nach etwas einigermaßen großen";
	mes "Metallgegenständen zu schauen.\"";
        close;

L_WhereLeather:
        mes "[Nicholas]";
	mes "\"Hmm. Du solltest nach jemandem suchen der";
	mes "mit Leder umgehen kann. Es gibt Gerüchte dass";
	mes "es jemanden in der östlichen Wüste gibt der";
	mes "das kann, Aber ich war nie dort.\"";
        close;


L_SetzerQuest:
        mes "[Nicholas]";
        if (@Q_SETZER_status & @SETZER_FLAG_MADE_SETZER)
        	mes "\"Noch eins?  Sicher, warum nicht.\"";
	mes "Nicholas begutachtet Dein Kurzschwert und nickt.";
        mes "\"Das ist gute Qualität. Ich kann daraus etwas";
	mes "besonderes machen, für fünf Brocken Eisenerz";
	mes "und 50,000 € -- aber ich werde auch eine";
	mes "Flasche Monster Öl brauchen.\"";
        if (@Q_SETZER_status < @SETZER_KNOWS_OIL)
        	set @Q_SETZER_status, @SETZER_KNOWS_OIL;
        callsub L_Update_Var;
        next;

        menu "Hier nimm.", -,
             "Monster Öl?  Was ist das?", L_ExplainMonsterOil,
             "WIE viel?  Vergiss es.", L_Pass;

	if (Zeny < 50000) goto L_SetzerNoZeny;
	if (countitem("IronOre") < 5) goto L_SetzerNoOre;
	if (countitem("MonsterOilPotion") < 1) goto L_SetzerNoMonsterOil;
	if (countitem("ShortSword") < 1) goto L_SetzerNoSword;
	// Kein inventory check benötigt, da das Kurzschwert entfernt wird, wird ein slot frei

        mes "[Nicholas]";
        mes "Nicholas nimmt die Gegenstände, heizt Dein";
	mes "Schwert auf und bearbeitet es mit einem";
	mes "schweren Hammer. Vor Deinen Augen wird es";
	mes "dünner und flacher. Schließlich gießt er das";
	mes "Monster Öl darüber, heizt es noch einmal auf";
	mes "und taucht es in Wasser.";

        Zeny = Zeny - 50000;
        delitem "IronOre", 5;
        delitem "MonsterOilPotion", 1;
        delitem "ShortSword", 1;

        getitem "Setzer", 1;
        if (!(@Q_SETZER_status & @SETZER_FLAG_MADE_SETZER))
		mes "[" + @SETZER_XP + " experience points]";
        if (!(@Q_SETZER_status & @SETZER_FLAG_MADE_SETZER))
		getexp @SETZER_XP, 1;
        @Q_SETZER_status = @Q_SETZER_status | @SETZER_FLAG_MADE_SETZER;
        callsub L_Update_Var;
        next;

        mes "[Nicholas]";
        mes "Nicholas reicht Dir Das vollendete Schwert.";
	mes "Es liegt eigenartig leicht in der Hand.";
        mes "\"Ich habe es leichter und schneller gemacht,";
	mes "es sollte jetzt auch ein wenig besser";
	mes "schneiden. Diese Art Schwert nennt man Setzer,";
	mes "nach einem berühmten Spieler der es als Waffe";
	mes "zwischen Schwert und Messer erfunden hat.\"";
        next;

        mes "[Nicholas]";
        mes "\"Ich bin recht zufrieden mit dem hier. Sei";
	mes "vorsichtig mit der Schneide, die ist ziemlich";
	mes "scharf.\"";
        close;

L_ExplainMonsterOil:
        mes "[Nicholas]";
        mes "\"Monster Öl ist ein spezielles Öl das benutzt";
	mes "werden kann um dünne Metallbleche zu härten,";
	mes "wenn es richtig benutzt wird. Unglücklicherweise";
	mes "ist es sehr schwer zu bekommen. Vielleicht";
	mes "kann Dir einer unserer Einheimischen Alchemisten";
	mes "dabei helfen?\"";
        close;

L_SetzerNoZeny:
        mes "[Nicholas]";
        mes "\"Tut mir leid, aber ich muss Dich bitten";
	mes "50,000 € zu zahlen; die art Waffe an die ich";
	mes "denke ist nicht einfach herzustellen.\"";
        close;

L_SetzerNoOre:
        mes "[Nicholas]";
        mes "\"Ohne fünf Klumpen Eisenerz kann ich Dein";
	mes "Schwert nicht aufbessern.\"";
        close;

L_SetzerNoMonsterOil:
        mes "[Nicholas]";
        mes "\"Ich weiß dass Monster Öl schwierig zu";
	mes "bekommen ist, aber ohne es kann ich dir nicht";
	mes "weiterhelfen. Frage einen Alchemisten wenn Du";
	mes "Hilfe bei der Herstellung brauchst.\"";
        close;

L_SetzerNoSword:
        mes "[Nicholas]";
        mes "\"Ich werde Dein altes Kurzschwert als Basis";
	mes "brauchen. Bitte bring es mir erst.\"";
        close;

L_End:
        close;

L_Done:
	mes "[Nicholas]";
	mes "\"Bitte sehr!\"";
	mes "";
	mes "\"Du kannst jederzeit wieder kommen.\"";
	close;

L_Update_Var:
        set QUEST_Forestbow_state,
        	(QUEST_Forestbow_state & ~(@Q_SHIELD_MASK | Q_SETZER_MASK)
                | (@Q_SHIELD_status << @Q_SHIELD_SHIFT)
                | (@Q_SETZER_status << @Q_SETZER_SHIFT));
        return;

L_TooMany:
        mes "[Nicholas]";
        mes "\"Du hast nicht genügend Platz dafür. Komm'";
	mes "später wieder wenn Du Platz hast.\"";
	close;
}
