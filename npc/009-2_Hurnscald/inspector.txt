
009-2,24,99,0	script	[Inspector]#Hurnscald	NPC150,{
	if (Inspector == 0 && BaseLevel >= 30) goto L_NohMask_Start;
	if (Inspector == 1) goto L_NohMask_AskVillage;
	if (Inspector == 2) goto L_NohMask_OldWoman;
	if (Inspector == 3) goto L_NohMask_TheaterMask;
	if (Inspector == 4) goto L_NohMask_TravT;
	if (Inspector == 5) goto L_NohMask_OldMan;
	if (Inspector == 6) goto L_NohMask_OldMan_2;
	if (Inspector == 7) goto L_NohMask_Alibi;
	if (Inspector == 8) goto L_NohMask_Alibi_2;
	if (Inspector == 9) goto L_NohMask_Alibi_3;
	if (Inspector == 10) goto L_NohMask_Satchel;
	if (Inspector == 11) goto L_NohMask_Basement;
	if (Inspector == 12) goto L_NohMask_Note;
	if (Inspector == 13) goto L_NohMask_TravT_2;
	if (Inspector == 14) goto L_NohMask_Over;
	if (Inspector == 15) goto L_NohMask_End;
	mes "[Inspector]";
	mes "\"Es tut mir leid, aber ich bin mit der Reihe an Raubüberfällen beschäftigt.\"";
	close;

L_NohMask_Start:
	mes "\"Hmm... was tun.\"";
	mes "Er schaut hoch in dein Gesicht.";
	next;

	mes "[Inspector]";
	mes "\"Du siehst tauglich aus. könntest du mir bem lösen der Raubüberfälle helfen?\"";
	next;

	menu
		"Ja", L_NohMask_Accept,
		"Nein", -;
	close;

L_NohMask_Accept:
	set Inspector, 1;
	mes "[Inspector]";
	mes "\"Dann OK. Ich bräuchte dich um die Dorfbewohner über die Überfälle zu befragen.\"";
	close;

L_NohMask_AskVillage:
	mes "[Inspector]";
	mes "\"Bitte befrage weiter die Anwohner.\"";
	close;

L_NohMask_OldWoman:
	mes "[Inspector]";
	mes "\"Hm...Ich weiß nicht ob ich ihrem Augenlicht glauben soll oder nicht. Schau ob jemand etwas weiß.\"";
	close;

L_NohMask_TheaterMask:
	set Inspector, 4;
	mes "[Inspector]";
	mes "\"Jemand in einer TheaterMaske, eh? Es war vor kurzem eine Reise Theatergruppe in der Stadt, aber diese sind Nach Tulimshar weiter. Gehe bitte und sprich mit ihrem Führer\"";
	close;

L_NohMask_TravT:
	mes "[Inspector]";
	mes "\"Bitte geh zur Theater Truppe um sie nach der Maske zu fragen.\"";
	close;

L_NohMask_OldMan:
	set Inspector, 6;
	mes "[Inspector]";
	mes "\"Hm...Ein alter Mann? könntest du ihn für mich verhören?\"";
	close;

L_NohMask_OldMan_2:
	mes "[Inspector]";
	mes "\"Hast du mit dem alten Mann gesprochen?\"";
	close;

L_NohMask_Alibi:
	mes "[Inspector]";
	mes "\"Könntest du seine Frau auch noch verhören?\"";
	close;

L_NohMask_Alibi_2:
	set Inspector, 9;
	mes "[Inspector]";
	mes "\"Hm... dann ist es nicht möglich. Ich bin mir nicht sicher, vielleicht findest du noch etwas anderes. Versuch doch bitte noch einmal mit jedem zu sprechen.\"";
	close;

L_NohMask_Alibi_3:
	mes "[Inspector]";
	mes "\"noch kein Fortschritt?\"";
	close;

L_NohMask_Satchel:
	mes "[Inspector]";
	mes "\"Dann geh in den Norden und Untersuche dort!\"";
	close;

L_NohMask_Basement:
	mes "[Inspector]";
	mes "\"Hast du in den Keller gesehen?\"";
	close;

L_NohMask_Note:
	set Inspector, 13;
	mes "[Inspector]";
	mes "\"Sehr seltsame. bring bitte die Maske zurück zum TruppenLeiter.\"";
	mes "";
	mes "\Auf dem weg... Steh Still! ich melde mich bei dir\"";
	close;

L_NohMask_TravT_2:
	mes "[Inspector]";
	mes "\"Bring bitte die Maske zurück zur Truppe.\"";
	close;

L_NohMask_Over:
	set Inspector, 15;
	mes "[Inspector]";
	mes "\"Mein Man hat alle gegenstände gefunden die gestohlen wurden. Sie waren alle in der Bergbau-Zeltlager. Wir wissen immer noch nicht, wer es war.\"";
	mes "[2500 experience points]";
	getexp 2500, 0;
	close;

L_NohMask_End:
	mes "[Inspector]";
	mes "\"Gute Arbeit, vielleicht brauch ich dich in einem anderem Fall.\"";
	close;
}
