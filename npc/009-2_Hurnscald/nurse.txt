
009-2,147,65,0	script	[Nurse]	NPC119,{
	mes "[Nurse]";
	mes "\"Kann ich dir Helfen?\"";
	next;

	if (Inspector == 1)
		menu
			"Auuu, diese Wunde! Die tut sooo Weh!", L_Heal,
			"Ich fühle mich nicht so gut, vielleicht bin ich krank.", L_Doctor,
			"Haben Sie etwas Außergewöhnliches gesehn?", L_NohMask,
			"Nein, mir geht's gut.", -;
	if (Inspector != 1)
		menu
			"Auuu, diese Wunde! Die tut sooo Weh!", L_Heal,
			"Ich fühle mich nicht so gut, vielleicht bin ich krank.", L_Doctor,
			"Nein, mir geht's gut.", -;
	mes "[Nurse]";
	mes "\"Dann muss ich dich aufvordern zu gehen... es gibt hier leute die wirklich meine hilfe brauchen!\"";
	close;

L_Doctor:
	mes "[Nurse]";
	mes "\"Dann sollten Sie besser zum Arzt. Er ist in der Regel in seinem Büro im 3. Stock.\"";
	close;

L_Heal:
	if (BaseLevel > 10) goto L_NoHeal;
	mes "[Nurse]";
	mes "\"Hier las mich dich Heilen.\"";
	next;
	heal 10000, 10000;
	close;

L_NoHeal:
	mes "[Nurse]";
	mes "\"Tut mir leid, ich bin nur hier um jungen leuten zu helfen.";
	mes "Dein Level ist schon über 10.";
	mes "Hier in der nähe ist ein Inn, da kannst du dich ausruhen.\"";
	close;

L_NohMask:
	mes "[Nurse]";
	mes "\"Ich bin zu beschäftigt um in die Stadt zu gehen.\"";
	close;
}
