009-2,124,27,0 script Ernst 135, {

mes "M�chtest du mit hilfe eines Kraftstein ein item +en?";
mes "beim versuch es zu plussen kann der gegenstand zerst�rt werden!";
menu
	"Jeans Chaps",L_JeansChaps,
	"Chainmail Shirt",L_CainmailShirt,
	"Light Platemail",L_LightPlatemail,
	"Warlord Plate",L_WarlordPlate,
	"Miner Gloves",L_MinerGloves,
	"Boots",L_Boots,
	"Knife",L_Knife,
	"Sharp Knife",L_SharpKnife,
	"Dagger",L_Dagger,
	"Short Sword",L_ShortSword,
	"Bone Knife",L_BoneKnife,
	"Setzer",L_Setzer,
	"Warlord Helmet",L_WarlordHelmet,
	"Knights Helmet",L_KnightsHelmet,
	"InfantryHelmet",L_InfantryHelmet,
	"CrusadeHelmet",L_CrusadeHelmet,
	"Savior Pants",L_SaviorPants,
	"Savior Armor",L_SaviorArmor,
	"Dragon Shield",L_DragonShield,
	"Nevermind",-;
close;



L_JeansChaps:
	mes "Wie viel + hat das item?";
	menu
		"0",L_JeansChaps0,
		"+1",L_JeansChaps1,
		"+2",L_JeansChaps2,
		"+3",L_JeansChaps3,
		"+4",L_uber,
	close;

L_JeansChaps0:
	set @normal,642;
	set @drunter,642;
	set @neues,3000;
	set @name$, "Jeans Chaps";
	goto L_frage;
L_JeansChaps1:
	set @normal,3000;
	set @drunter,642;
	set @neues,3001;
	set @name$, "Jeans Chaps+1";
	goto L_frage;
L_JeansChaps2:
	set @normal,3001;
	set @drunter,3000;
	set @neues,3002;
	set @name$, "Jeans Chaps+2";
	goto L_frage;
L_JeansChaps3:
	set @normal,3002;
	set @drunter,3001;
	set @neues,3003;
	set @name$, "Jeans Chaps+3";
	goto L_frage;



L_CainmailShirt:	
	menu
		"0",L_CainmailShirt0,
		"+1",L_CainmailShirt1,
		"+2",L_CainmailShirt2,
		"+3",L_CainmailShirt3,
		"+4",L_uber,
	close;

L_CainmailShirt0:
	set @normal,625;
	set @drunter,625;
	set @neues,3010;
	set @name$, "Chainmail Shirt";
	goto L_frage;
L_CainmailShirt1:
	set @normal,3010;
	set @drunter,625;
	set @neues,3011;
	set @name$, "Chainmail Shirt+1";
	goto L_frage;
L_CainmailShirt2:
	set @normal,3011;
	set @drunter,3010;
	set @neues,3012;
	set @name$, "Chainmail Shirt +2";
	goto L_frage;
L_CainmailShirt3:
	set @normal,3012;
	set @drunter,3011;
	set @neues,3013;
	set @name$, "Chainmail Shirt +3";
	goto L_frage;


L_LightPlatemail:
	
	menu
		"0",L_LightPlatemail0,
		"+1",L_LightPlatemail1,
		"+2",L_LightPlatemail2,
		"+3",L_LightPlatemail3,
		"+4",L_uber,
	close;

L_LightPlatemail0:
	set @normal,626;
	set @drunter,626;
	set @neues,3020;
	set @name$, "Light Platemail";
	goto L_frage;
L_LightPlatemail1:
	set @normal,3020;
	set @drunter,626;
	set @neues,3021;
	set @name$, "Light Platemail +1";
	goto L_frage;
L_LightPlatemail2:
	set @normal,3021;
	set @drunter,3020;
	set @neues,3022;
	set @name$, "Light Platemail +2";
	goto L_frage;
L_LightPlatemail3:
	set @normal,3022;
	set @drunter,3021;
	set @neues,3023;
	set @name$, "Light Platemail +3";
	goto L_frage;


L_WarlordPlate:
	
	menu
		"0",L_WarlordPlate0,
		"+1",L_WarlordPlate1,
		"+2",L_WarlordPlate2,
		"+3",L_WarlordPlate3,
		"+4",L_uber,
	close;

L_WarlordPlate0:
	set @normal,658;
	set @drunter,658;
	set @neues,3030;
	set @name$, "Warlord Plate";
	goto L_frage;
L_WarlordPlate1:
	set @normal,3030;
	set @drunter,658;
	set @neues,3031;
	set @name$, "Warlord Plate +1";
	goto L_frage;
L_WarlordPlate2:
	set @normal,3031;
	set @drunter,3030;
	set @neues,3032;
	set @name$, "Warlord Plate +2";
	goto L_frage;
L_WarlordPlate3:
	set @normal,3032;
	set @drunter,3031;
	set @neues,3033;
	set @name$, "Warlord Plate +3";
	goto L_frage;


L_MinerGloves:
	
	menu
		"0",L_MinerGloves0,
		"+1",L_MinerGloves1,
		"+2",L_MinerGloves2,
		"+3",L_MinerGloves3,
		"+4",L_uber,
	close;

L_MinerGloves0:
	set @normal,531;
	set @drunter,531;
	set @neues,3040;
	set @name$, "MinerGloves";
	goto L_frage;
L_MinerGloves1:
	set @normal,3040;
	set @drunter,531;
	set @neues,3041;
	set @name$, "MinerGloves +1";
	goto L_frage;
L_MinerGloves2:
	set @normal,3041;
	set @drunter,3040;
	set @neues,3042;
	set @name$, "MinerGloves +2";
	goto L_frage;
L_MinerGloves3:
	set @normal,3042;
	set @drunter,3041;
	set @neues,3043;
	set @name$, "MinerGloves +3";
	goto L_frage;


L_Boots:
	
	menu
		"0",L_Boots0,
		"+1",L_Boots1,
		"+2",L_Boots2,
		"+3",L_Boots3,
		"+4",L_uber,
	close;

L_Boots0:
	set @normal,528;
	set @drunter,528;
	set @neues,3050;
	set @name$, "Boots";
	goto L_frage;
L_Boots1:
	set @normal,3050;
	set @drunter,528;
	set @neues,3051;
	set @name$, "Boots +1";
	goto L_frage;
L_Boots2:
	set @normal,3051;
	set @drunter,3050;
	set @neues,3052;
	set @name$, "Boots +2";
	goto L_frage;
L_Boots3:
	set @normal,3052;
	set @drunter,3051;
	set @neues,3053;
	set @name$, "Boots +3";
	goto L_frage;


L_Knife:
	
	menu
		"0",L_Knife0,
		"+1",L_Knife1,
		"+2",L_Knife2,
		"+3",L_Knife3,
		"+4",L_uber,
	close;

L_Knife0:
	set @normal,1201;
	set @drunter,1201;
	set @neues,3060;
	set @name$, "Knife";
	goto L_frage;
L_Knife1:
	set @normal,3060;
	set @drunter,1201;
	set @neues,3061;
	set @name$, "Knife +1";
	goto L_frage;
L_Knife2:
	set @normal,3061;
	set @drunter,3060;
	set @neues,3062;
	set @name$, "Knife +2";
	goto L_frage;
L_Knife3:
	set @normal,3062;
	set @drunter,3061;
	set @neues,3063;
	set @name$, "Knife +3";
	goto L_frage;


L_SharpKnife:
	
	menu
		"0",L_SharpKnife0,
		"+1",L_SharpKnife1,
		"+2",L_SharpKnife2,
		"+3",L_SharpKnife3,
		"+4",L_uber,
	close;

L_SharpKnife0:
	set @normal,522;
	set @drunter,522;
	set @neues,3070;
	set @name$, "Sharp Knife";
	goto L_frage;
L_SharpKnife1:
	set @normal,3070;
	set @drunter,522;
	set @neues,3071;
	set @name$, "Sharp Knife +1";
	goto L_frage;
L_SharpKnife2:
	set @normal,3071;
	set @drunter,3070;
	set @neues,3072;
	set @name$, "Sharp Knife +2";
	goto L_frage;
L_SharpKnife3:
	set @normal,3072;
	set @drunter,3071;
	set @neues,3073;
	set @name$, "Sharp Knife +3";
	goto L_frage;


L_Dagger:
	
	menu
		"0",L_Dagger0,
		"+1",L_Dagger1,
		"+2",L_Dagger2,
		"+3",L_Dagger3,
		"+4",L_uber,
	close;

L_Dagger0:
	set @normal,521;
	set @drunter,521;
	set @neues,3080;
	set @name$, "Dagger";
	goto L_frage;
L_Dagger1:
	set @normal,3080;
	set @drunter,521;
	set @neues,3081;
	set @name$, "Dagger +1";
	goto L_frage;
L_Dagger2:
	set @normal,3081;
	set @drunter,3080;
	set @neues,3082;
	set @name$, "Dagger +2";
	goto L_frage;
L_Dagger3:
	set @normal,3082;
	set @drunter,3081;
	set @neues,3083;
	set @name$, "Dagger +3";
	goto L_frage;


L_ShortSword:
	
	menu
		"0",L_ShortSword0,
		"+1",L_ShortSword1,
		"+2",L_ShortSword2,
		"+3",L_ShortSword3,
		"+4",L_uber,
	close;

L_ShortSword0:
	set @normal,536;
	set @drunter,536;
	set @neues,3090;
	set @name$, "Short Sword";
	goto L_frage;
L_ShortSword1:
	set @normal,3090;
	set @drunter,536;
	set @neues,3091;
	set @name$, "Short Sword +1";
	goto L_frage;
L_ShortSword2:
	set @normal,3091;
	set @drunter,3090;
	set @neues,3092;
	set @name$, "Short Sword +2";
	goto L_frage;
L_ShortSword3:
	set @normal,3092;
	set @drunter,3091;
	set @neues,3093;
	set @name$, "Short Sword +3";
	goto L_frage;


L_BoneKnife:
	
	menu
		"0",L_BoneKnife0,
		"+1",L_BoneKnife1,
		"+2",L_BoneKnife2,
		"+3",L_BoneKnife3,
		"+4",L_uber,
	close;

L_BoneKnife0:
	set @normal,570;
	set @drunter,570;
	set @neues,3100;
	set @name$, "Bone Knife";
	goto L_frage;
L_BoneKnife1:
	set @normal,3100;
	set @drunter,570;
	set @neues,3101;
	set @name$, "Bone Knife +1";
	goto L_frage;
L_BoneKnife2:
	set @normal,3101;
	set @drunter,3100;
	set @neues,3102;
	set @name$, "Bone Knife +2";
	goto L_frage;
L_BoneKnife3:
	set @normal,3102;
	set @drunter,3101;
	set @neues,3103;
	set @name$, "Bone Knife +3";
	goto L_frage;


L_Setzer:
	
	menu
		"0",L_Setzer0,
		"+1",L_Setzer1,
		"+2",L_Setzer2,
		"+3",L_Setzer3,
		"+4",L_uber,
	close;

L_Setzer0:
	set @normal,571;
	set @drunter,571;
	set @neues,3110;
	set @name$, "Setzer";
	goto L_frage;
L_Setzer1:
	set @normal,3110;
	set @drunter,571;
	set @neues,3111;
	set @name$, "Setzer +1";
	goto L_frage;
L_Setzer2:
	set @normal,3111;
	set @drunter,3110;
	set @neues,3112;
	set @name$, "Setzer +2";
	goto L_frage;
L_Setzer3:
	set @normal,3112;
	set @drunter,3111;
	set @neues,3113;
	set @name$, "Setzer +3";
	goto L_frage;



L_WarlordHelmet:
	
	menu
		"0",L_WarlordHelmet0,
		"+1",L_WarlordHelmet1,
		"+2",L_WarlordHelmet2,
		"+3",L_WarlordHelmet3,
		"+4",L_uber,
	close;

L_WarlordHelmet0:
	set @normal,636;
	set @drunter,636;
	set @neues,3120;
	set @name$, "Warlord Helmet";
	goto L_frage;
L_WarlordHelmet1:
	set @normal,3120;
	set @drunter,636;
	set @neues,3121;
	set @name$, "Warlord Helmet +1";
	goto L_frage;
L_WarlordHelmet2:
	set @normal,3121;
	set @drunter,3120;
	set @neues,3122;
	set @name$, "Warlord Helmet +2";
	goto L_frage;
L_WarlordHelmet3:
	set @normal,3122;
	set @drunter,3121;
	set @neues,3123;
	set @name$, "Warlord Helmet +3";
	goto L_frage;



L_KnightsHelmet:
	
	menu
		"0",L_KnightsHelmet0,
		"+1",L_KnightsHelmet1,
		"+2",L_KnightsHelmet2,
		"+3",L_KnightsHelmet3,
		"+4",L_uber,
	close;

L_KnightsHelmet0:
	set @normal,637;
	set @drunter,637;
	set @neues,3130;
	set @name$, "Knights Helmet";
	goto L_frage;
L_KnightsHelmet1:
	set @normal,3130;
	set @drunter,637;
	set @neues,3131;
	set @name$, "Knights Helmet +1";
	goto L_frage;
L_KnightsHelmet2:
	set @normal,3131;
	set @drunter,3130;
	set @neues,3132;
	set @name$, "Knights Helmet +2";
	goto L_frage;
L_KnightsHelmet3:
	set @normal,3132;
	set @drunter,3131;
	set @neues,3133;
	set @name$, "Knights Helmet +3";
	goto L_frage;


L_InfantryHelmet:
	
	menu
		"0",L_InfantryHelmet0,
		"+1",L_InfantryHelmet1,
		"+2",L_InfantryHelmet2,
		"+3",L_InfantryHelmet3,
		"+4",L_uber,
	close;

L_InfantryHelmet0:
	set @normal,638;
	set @drunter,638;
	set @neues,3140;
	set @name$, "Infantry Helmet";
	goto L_frage;
L_InfantryHelmet1:
	set @normal,3140;
	set @drunter,638;
	set @neues,3141;
	set @name$, "Infantry Helmet +1";
	goto L_frage;
L_InfantryHelmet2:
	set @normal,3141;
	set @drunter,3140;
	set @neues,3142;
	set @name$, "Infantry Helmet +2";
	goto L_frage;
L_InfantryHelmet3:
	set @normal,3142;
	set @drunter,3141;
	set @neues,3143;
	set @name$, "Infantry Helmet +3";
	goto L_frage;


L_CrusadeHelmet:
	
	menu
		"0",L_CrusadeHelmet0,
		"+1",L_CrusadeHelmet1,
		"+2",L_CrusadeHelmet2,
		"+3",L_CrusadeHelmet3,
		"+4",L_uber,
	close;

L_CrusadeHelmet0:
	set @normal,639;
	set @drunter,639;
	set @neues,3150;
	set @name$, "Crusade Helmet";
	goto L_frage;
L_CrusadeHelmet1:
	set @normal,3150;
	set @drunter,639;
	set @neues,3151;
	set @name$, "Crusade Helmet +1";
	goto L_frage;
L_CrusadeHelmet2:
	set @normal,3151;
	set @drunter,3150;
	set @neues,3152;
	set @name$, "Crusade Helmet +2";
	goto L_frage;
L_CrusadeHelmet3:
	set @normal,3152;
	set @drunter,3151;
	set @neues,3153;
	set @name$, "Crusade Helmet +3";
	goto L_frage;


L_SaviorPants:
	menu
		"0",L_SaviorPants0,
		"+1",L_SaviorPants1,
		"+2",L_SaviorPants2,
		"+3",L_SaviorPants3,
		"+4",L_uber,
	close;

L_SaviorPants0:
	set @normal,2209;
	set @drunter,2209;
	set @neues,3160;
	set @name$, "Savior Pants";
	goto L_frage;
L_SaviorPants1:
	set @normal,3160;
	set @drunter,2209;
	set @neues,3161;
	set @name$, "Savior Pants +1";
	goto L_frage;
L_SaviorPants2:
	set @normal,3161;
	set @drunter,3160;
	set @neues,3162;
	set @name$, "Savior Pants +2";
	goto L_frage;
L_SaviorPants3:
	set @normal,3162;
	set @drunter,3161;
	set @neues,3163;
	set @name$, "Savior Pants +3";
	goto L_frage;


L_SaviorArmor:

	menu
		"0",L_SaviorArmor0,
		"+1",L_SaviorArmor1,
		"+2",L_SaviorArmor2,
		"+3",L_SaviorArmor3,
		"+4",L_uber,
	close;

L_SaviorArmor0:
	set @normal,2213;
	set @drunter,2213;
	set @neues,3170;
	set @name$, "Savior Armor";
	goto L_frage;
L_SaviorArmor1:
	set @normal,3170;
	set @drunter,2213;
	set @neues,3171;
	set @name$, "Savior Armor +1";
	goto L_frage;
L_SaviorArmor2:
	set @normal,3171;
	set @drunter,3170;
	set @neues,3172;
	set @name$, "Savior Armor +2";
	goto L_frage;
L_SaviorArmor3:
	set @normal,3172;
	set @drunter,3171;
	set @neues,3173;
	set @name$, "Savior Armor +3";
	goto L_frage;


L_DragonShield:

	menu
		"0",L_DragonShield0,
		"+1",L_DragonShield1,
		"+2",L_DragonShield2,
		"+3",L_DragonShield3,
		"+4",L_uber,
	close;

L_DragonShield0:
	set @normal,2214;
	set @drunter,2214;
	set @neues,3180;
	set @name$, "Dragon Shield";
	goto L_frage;
L_DragonShield1:
	set @normal,3180;
	set @drunter,2214;
	set @neues,3151;
	set @name$, "Dragon Shield +1";
	goto L_frage;
L_DragonShield2:
	set @normal,3181;
	set @drunter,3180;
	set @neues,3182;
	set @name$, "Dragon Shield +2";
	goto L_frage;
L_DragonShield3:
	set @normal,3182;
	set @drunter,3181;
	set @neues,3183;
	set @name$, "Dragon Shield +3";
	goto L_frage;





L_frage:
	if (countitem(@normal) == 0) goto L_keinitem;
	if (countitem("Kraftstein")==0) goto L_keinks;
	mes "willst du das item wirklich +en?";
	mes "es kann fehlschlagen und Zerst�rt werden";
		menu
			"Ja",L_randsystem,
			"Nein",-;
	close;

L_randsystem:		
	set @temp,rand(6);
		if(@temp == 0) goto L_defect; 	// 16%	
		if(@temp == 1) goto L_plus;	// 16%	
		if(@temp == 2) goto L_minus;	// 16%	
		if(@temp == 3) goto L_plus;	// 16%	
		if(@temp == 4) goto L_bleibt;	// 16%	
		if(@temp == 5) goto L_plus;	// 16%	


L_defect:
	mes "Die Verbesserung ist fehlgeschlagen. Der Gegenstand wurde zerst�rt";
	delitem @normal,1;
	delitem "Kraftstein",1;
	close;
L_plus:
	mes "Die Verbesserung ist erfolgreich. Die Werte des Gegenstands wurden um 1 erh�ht";
	delitem @normal,1;
	getitem @neues,1;
	delitem "Kraftstein",1;
	close;
L_minus:
	mes "Die Verbesserung ist fehlgeschlagen. die Werte des Gegenstands wurden um 1 reduziert";
	delitem @normal,1;
	getitem @drunter,1;
	delitem "Kraftstein",1;
	close;

L_bleibt:
	mes "Die Verbesserung ist fehlgeschlagen, aber es ist keine veraenderung zu bemerken";
	delitem "Kraftstein",1;
	close;		



L_uber:
	mes "das item ist schon auf +4, und kann nicht weiter erh�ht werden.";
	mes "komm bald wieder!";
	close;
L_keinitem:
	mes "Du besitzt kein" + @name$ + "komm spaeter wieder!";
	close;
L_keinks:
	mes "du hast keinen KraftStein";
	close;
}