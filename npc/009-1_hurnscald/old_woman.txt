
009-1,29,43,0	script	[Old Woman]	NPC154,{
	if (BaseLevel < 40) goto L_Lower;

	mes "[Alte Frau]";
	mes "\"Hallo.\"";
	next;

L_Continue:
	if ((Inspector >= 1  && Inspector <= 7) || Inspector == 9) goto L_NohMask;
	close;

L_Lower:
	mes "[Alte Frau]";
	mes "\"Pass' bitte auf diese Blumen auf. Sie sind nicht sehr freundlich.\"";
	next;
	goto L_Continue;

L_NohMask:
	if (Inspector == 7) goto L_NohMask_Alibi;
	callfunc "ProcessEquip";
	if (@torsoC == cDarkBlue && @legsC == cDarkBlue) goto L_NohMask_Fake;
	if (Inspector >= 3 && Inspector <= 6) close;

	menu
		"Hast du irgendetwas ungewöhnliches in letzter Zeit bemerkt?", L_NohMask_FirstAsk,
		"Weißt du etwas über die Überfälle in der letzten Zeit?", L_NohMask_FirstAsk,
		"Hallo.", -;
	close;

L_NohMask_FirstAsk:
	mes "[Alte Frau]";
	mes "\"Ja, aber ich rede nur mit dem Inspektor persönlich!\"";
	if (Inspector == 1) set Inspector, 2;
	close;

L_NohMask_Alibi:
	menu
		"War dein Mann mit dir daheim, als die Theatertruppe da war?", -;

	mes "[Alte Frau]";
	mes "\"Ja, wir waren beide daheim.\"";
	set Inspector, 8;
	close;

L_NohMask_Fake:
	if (Inspector == 9) goto L_NohMask_Fake_Satchel;
	if (Inspector >= 3 && Inspector <= 6) goto L_NohMask_Filler;

	mes "[Alte Frau]";
	mes "\"Ich habe jemanden in der Stadt herumschleichen gesehen mit einer Theater Maske. Sie sah aus wie die Masken der Truppe, die letztens in der Stadt war.\"";
	set Inspector, 3;
	close;

L_NohMask_Filler:
	mes "[Alte Frau]";
	mes "\"Ich hoffe, du wirst diese Person finden!\"";
	close;

L_NohMask_Fake_Satchel:
	mes "[Alte Frau]";
	mes "\"Mir ist noch etwas eingefallen. In der Nacht, als die Truppe die Stadt verließ, sah ich einen Mann mit Theatermaske und einer Art Schulranzen durch die Stadt rennen. Er ging in Richtung Norden.\"";
	set Inspector, 10;
	close;
}
