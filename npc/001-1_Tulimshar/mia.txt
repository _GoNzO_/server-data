
001-1,52,45,0	script	[Mia]	NPC210,{

	if (Noob_Quest == 6) goto L_Fertig;
	if (Noob_Quest == 5) goto L_Skorpion;
	if (Noob_Quest == 4) goto L_MadenErledigt;
	if (Noob_Quest == 3) goto L_Maden;
	if (Noob_Quest == 2) goto L_BeinErledigt;
	if (Noob_Quest == 1) goto L_Beinchen;

	mes "Du siehst eine junge Händlerin die sich umschaut";
	mes "als würde sie etwas suchen. Sie macht einen";
	mes "hektischen Eindruck auf dich. Schließlich bemerkt";
	mes "sie deine Anwesenheit und spricht dich an.";
	next;

	mes "[Mia]";
	mes "\"Hallo Fremder.\"";

	menu
		"Guten Tag, mein Name ist "+strcharinfo(0)+".", -;

	mes "[Mia]";
	mes "\"Und ich bin Mia. Kannst du mir vielleicht";
	mes "helfen?\"";

	menu
		"Ist dir denn noch zu helfen?", L_Close,
		"Ich werds versuchen, was kann ich tun?", -;

	mes "[Mia]";
	mes "\"Vor ein paar Tagen kam ein spezieller Kunde\"";
	mes "\"bei uns vorbei und hat meinen Mann und mich\"";
	mes "\"darum gebeten ein paar Sachen zu organisieren.\"";
	mes "\"Er wirkte sehr wohlhabend und meinte dass er\"";
	mes "\"sich finanziell sehr erkenntlich zeigen würde\"";
	mes "\"wenn wir den Auftrag annehmen.\"";

	menu
		"Und wo genau liegt das Problem?", -;

	mes "[Mia]";
	mes "\"Ich bin unter Zeitdruck. Der Kunde wird seine\"";
	mes "\"Bestellung morgen abholen wollen und mir fehlen\"";
	mes "\"noch ein paar Zutaten die ich noch nicht habe\"";
	mes "\"weil ich mich zu sehr davor ekel.\"";
	next;
	mes "[Mia]";
	mes "\"Wenn du mir hilfst sie herbei zu bringen könnte\"";
	mes "\"ich dir als Dank ein paar Waren aus meinem Shop\"";
	mes "\"zur Verfügung stellen. Na, haben wir einen Deal?\"";

	menu
		"Klingt gut. Was soll ich dir bringen?", -,
		"Dein Problem. Ich hab keine Zeit für sowas.", L_Close;

	mes "[Mia]";
	mes "\"Super\"";
	mes "";
	mes "Sie holt einen langen knittrigen Wisch auf dem";
	mes "die Bestellung steht";
	next;

	mes "[Mia]";
	mes "\"Als erstes brauche ich Beinchen von Insekten\"";
	mes "\"oder anderen Krabbelviechern, 10 an der Zahl\"";
	mes "\"sollten reichen.\"";

	menu
		"Schau ich hab welche.", L_Beinchen,
		"Schon so gut wie erledigt.", -;

	set Noob_Quest, 1;
	close;

L_Beinchen:
	if (countitem(518) < 10) goto L_Unvollst;
	mes "[Mia]";
	mes "\"Perfekt, die sind besonders schön. Hier nimm\"";
	mes "\"dieses Obst als Dank, aus kontrolliertem Anbau\"";
	mes "*zwinker*.";
	mes "(20 rote Äpfel, 20 Orangen und 200 exp)";
	delitem 518, 10;
	set Noob_Quest, 2;
	getitem 535, 20;
	getitem 657, 20;
	set BaseExp, BaseExp+200;
	next;

L_BeinErledigt:
	mes "[Mia]";
	mes "\"Als nächstes steht hier... 5 geröstete Maden\"";
	mes "*würg*";
	mes "\"und 10 Madenschleim.\"";

	menu
		"Hier bitteschön.", L_Maden,
		"Schon auf dem Weg.", -;

	set Noob_Quest, 3;
	close;

L_Maden:
	if (countitem(533) < 5) goto L_Unvollst;
	if (countitem(505) < 10) goto L_Unvollst;
	mes "[Mia]";
	mes "\"Hervorragend. Leg alles in die Kiste, ich\"";
	mes "\"ekel mich zu sehr davor. Ach bevor ich's\"";
	mes "\"vergesse, deine Belohnung für die Mühe. Hilft\"";
	mes "\"die Monster besser abzumurksen und ein Hut\"";
	mes "\"der dich vor der Mittagssonne schüzt.\"";
	mes "(Wüstenhut, Dolch und 200 exp)";
	delitem 533, 5;
	delitem 505, 10;
	set Noob_Quest, 4;
	getitem 723, 1;
	getitem 521, 1;
	set BaseExp, BaseExp+200;
	next;

L_MadenErledigt:
	mes "[Mia]";
	mes "\"Dann steht hier zu guter letzt\"";
	mes "\"10 Skorpionstachel.";

	menu
		"Ich kill die Skorpione für dich.", -,
		"Schau hier hab ich welche.", L_Skorpion;

	set Noob_Quest, 5;
	close;

L_Skorpion:
	if (countitem(507) < 10) goto L_Unvollst;
	mes "[Mia]";
	mes "\"Wunderbar du hast mir sehr geholfen.\"";
	mes "\"Hab 1000 Dank. Hier nimm das für deine\"";
	mes "\"Hilfsbereitschaft.\"";
	mes "(Baumwollhose, 5000 gp und 300 exp)";
	delitem 507, 10;
	set Noob_Quest, 6;
	getitem 586, 1;
	set Zeny, Zeny + 5000;
	set BaseExp, BaseExp+300;
	close;

L_Unvollst:
	mes "[Mia]";
	mes "\"Das reicht leider noch nicht ganz.\"";
	close;

L_Fertig:
	mes "[Mia]";
	mes "\"Hallo "+strcharinfo(0)+"!\"";

L_Close:
	close;

}
