
001-1,32,27,0	script	[Ian]	NPC102,4,4,{
	if (TUT_var & 1 == 1 && TUT_var & 2 == 0 && BaseLevel >= 10) callsub S_Grad;
	mes "[Ian der Helfer]";
	mes "\"Willst du über irgendwas Bescheid wissen?\"";
	next;

L_Menu_A:
	menu "Kämpfen", L_Fight,
             "Items", L_Items,
             "Monster", L_Monster,
             "Friseur", L_Style,
             "Quests", L_Quests,
             "NPCs", L_NPC,
             "Befehle", L_Comm,
             "Attribute", L_Stats,
             "Magie", L_Magic,
             "Schnelltasten", L_Key,
             "Regeln", L_Laws,
             "Ich weiß über alles Bescheid!", L_Know;
	close;

L_Fight:
	mes "[Ian der Helfer]";
	mes "\"Die Leute leben in dieser Welt mit dem Töten von Monstern.";
	mes "Du kannst die Monster und andere Spieler mit der [CTRL] Taste angreifen oder du klickst mit der linken Maustaste auf das Monster.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Du brauchst [CTRL] nicht ständing zu drücken. Dein Charakter wird automatisch weiter angreifen.";
	mes "Ich hoffe ich konnte dir hier behilflich sein.\"";
	next;
	goto L_Menu_A;

L_Items:
	mes "[Ian der Helfer]";
	mes "\"Es gibt 3 verschiedene Item-Typen.";
	mes "Es gibt Konsumierbare, Ausrüstbare, oder Questgegenstände\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Konsumierbare Items wie zum Beispiel Tränke können nur einmal verwendet werden.";
	mes "Nachdem sie verwendet wurden, verschwinden sie aus deinem Inventar.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Ausrüstungsgegenstände wie Rüstungen, Waffen, oder Accessories"; 
	mes "können zur Zierde angezogen werden oder um deinen Status zu erhöhen.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Andere Items wie Madenschleim, werden";
	mes "zum Handel benutzt oder um andere Sachen damit herzustellen. Du kannst sie auch verkaufen.\"";
	next;
	goto L_Menu_A;

L_Monster:
	mes "[Ian der Helfer]";
	mes "\"In jeder Welt sind Monster. Die Monster können überall gefunden werden!";
	mes "Wenn du wissen willst, wie man sie bekämpft, lies bitte den Abschnitt [Kämpfen].\"";
	next;
	mes "\"Es gibt verschiedene Typen von Monstern: Aggressive, neutrale, und Assistenten.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Aggressive Monster wissen, dass sie in Gefahr sind";
	mes "Also greifen sie sofort an, weil sie ihr Revier verteidigen wollen.";
	mes "Sie greifen jeden Spieler ohne Rücksicht an.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Neutrale Monster laufen einfach nur herum, wenn sie nicht angegriffen werden.";
	mes "Sie lassen jeden in Ruhe, wenn sie selbst in Ruhe gelassen werden.\"";
	next;
	mes "[Ian der Helfer]";
	mes "\"Assistenten sind Monster, die anderen Monstern helfen.";
	mes "Du solltest zuerst überprüfen, wieviele in deiner Nähe sind, bevor du einen einzelnen angreifst!\"";
	next;
	goto L_Menu_A;

L_Style:
	mes "[Ian der Helfer]";
	mes "\"Die Friseur-NPCs schneiden und färben deine Haare!";
	mes "Sie sind bekannt für Ihre Frisuren.\"";
	next;
	goto L_Menu_A;

L_Quests:
	mes "[Ian der Helfer]";
	mes "\"Es gibt Leute in dieser Welt, die deine Hilfe brauchen!";
	mes "Die meisten Leute halten sich nicht zurück dich zu belohnen, wenn du ihnen hilfst.";
	mes "Also sei nett und hilf den Leuten!\"";
	next;
	goto L_Menu_A;

L_NPC:
	mes "[Ian der Helfer]";
	mes "\"NPCs [Nicht spielende Charakter] sind Leute, die immer im Spiel sind,";
	mes "Es gibt viele Möglichkeiten mit ihnen zu reden und ihnen zu helfen.\"";
	next;
	goto L_Menu_A;

L_Comm:
	mes "[Ian der Helfer]";
	mes "\"/clear löscht die Nachrichten in der Nachrichten-Box.\"";
	mes "\"/help erklärt dir die Befehle (ein Einziges startet mit /) in der Chat-Box.\"";
	mes "\"/whisper [name] erlaubt dir, eine private Nachricht zu senden.\"";
	mes "\"/who zeigt dir die Anzahl Spieler, die gerade online sind.\"";
	mes "\"/where zeigt dir deine derzeitige Position.\"";
	next;
	goto L_Menu_A;

L_Stats:
	mes "[Ian der Helfer]";
	mes "\"Die Leute werden richtig stark mit Stärke, Beweglichkeit, Ausdauer, Intelligenz, Gesundheit und Glück.\"";
        next;
	mes "[Ian der Helfer]";
        mes "\"Die Stärke hilft dir, mehr Sachen zu tragen und außerdem verstärkt sich deine Angriffskraft. Aber es ist nicht sehr Hilfreich bei Fernwaffen.";
        mes "Mehr Beweglichkeit erlaubt es dir, schneller anzugreifen, und anderen Attacken mehr auszuweichen.";
        mes "Deine Ausdauer bestimmt, wie oft du ein Monster treffen kannst und wie effektiv du mit Fernwaffen bist.\"";
        next;
	mes "[Ian der Helfer]";
        mes "\"Die Gesundheit erhöht deine Lebenspunkte und reduziert den Schaden, den du erleidest.";
        mes "Intelligenz wird für Alchemie und Magie gebraucht, aber es gibt noch mehr Möglichkeiten, die Intelligenz zu nutzen.";
        mes "Glück entscheidet kleine Dinge, wie zum Beispiel kritische Treffer zu machen oder ihnen auszuweichen.\"";
        next;
	mes "[Ian der Helfer]";
        mes "\"Ich empfehle dir, deine Ausdauer zu trainieren, denn wenn du auf gefaehrliche Monster stößt, sind sie schwer zu treffen.";
        mes "Versuch an deinem Glück zu arbeiten und deine Intelligenz ist momentan nichts, was dich beschaeftigen müsste.\"";
        next;
        goto L_Menu_A;

L_Magic:
	mes "[Ian der Helfer]";
        mes "\"Ich weiß noch früher, da war ein magischer Samen im Süden von Tulimshar, wo die Leute magische Kräfte erlernen konnten. Doch leider ist der Samen irgendwie verschwunden.";
        mes "Wie auch immer, ich habe gehört, dass ein Barde letztens etwas darüber herausgefunden hat... Wenn du an Magie interessiert bist, solltest du mal mit ihm sprechen!\"";
        next;
        goto L_Menu_A;

L_Key:
	mes "[Ian der Helfer]";
	mes "\"Es gibt viele Tastenkombinationen, drücke F1 für eine kurze Liste!\"";
	next;
	goto L_Menu_A;

L_Laws:
	mes "[Ian der Helfer]";
        mes "\"Der zuständige Inspektor kann dir damit helfen.\"";
        next;
	goto L_Menu_A;
        
L_Know:
	close;

S_Grad:
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	mes "[Ian der Helfer]";
	mes "\"Hey, das hast du gut gemacht. Lass mich dir das hier geben.\"";

	getitem "GraduationCap", 1;
	set TUT_var, TUT_var | 2;
	next;

	return;

L_TooMany:
	mes "[Ian der Helfer]";
	mes "\"Ich wollte dir etwas geben, aber du hast keinen Platz dafür.\"";
	next;
	return;
    
OnTouch:
    if (TUT_var & 1 == 1) close;
	callfunc "GameRules";
	mes "Ian, der Helfer, kann dir helfen, das Spiel kennen zu lernen.";
	close;
}

