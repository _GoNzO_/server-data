
001-1,50,53,0	script	[Ishi]	NPC106,{
	if (MPQUEST == 0) goto L_Register;
	if (tvis == 0) set tvis, 1;
	if (Mobpt < tvis) goto L_NotEnough;

	setarray @items$, "AppleCake", "Arrow", "BatTeeth", "BatWing", "Beer", "BluePresentBox", "Boots", "BugLeg", "CactusDrink", "CactusPotion", "Cake", "Candy", "CandyCane", "CasinoCoins", "CaveSnakeLamp", "CaveSnakeTongue", "CherryCake", "ChickenLeg", "ChocolateBar", "ChocolateCake", "CottonBoots", "CottonCloth", "CottonShirt", "CottonShorts", "DecorCandy", "EasterEgg", "FancyHat", "GingerBreadMan", "GrassSnakeEgg", "GrassSnakeTongue", "GreenApple", "HardSpike", "IronOre", "Lifestone", "LightBlueDye", "MaggotSlime", "Milk", "MountainSnakeEgg", "MountainSnakeTongue", "Orange", "OrangeCake", "OrangeCupcake", "Petal", "PileOfAsh", "PinkAntenna", "PoltergeistPowder", "PurplePresentBox", "RawLog", "RedApple", "RedDye", "RedScorpionStinger", "ScorpionStinger", "SerfHat", "SmallHealingPotion", "SmallMushroom", "SnakeEgg", "SnakeSkin", "SnakeTongue", "SpectrePowder", "Steak", "TinyHealingPotion", "TreasureKey", "WhiteCake", "WhiteFur", "WispPowder", "XmasCake", "XmasCandyCane", "YellowDye", "AFKCap";

	mes "[Ishi die Geschenkverteilerin]";
	mes "\"Willkommen! Wie ich sehe, hast du " + Mobpt + " Monsterpunkte. Willst du einige davon gegen Items eintauschen?\"";
	next;
	
	set @i, 0;
	setarray @Menu$, "", "", "", "", "", "", "", "", "", "", "";
	callsub SUB_prep_menu;
	
	menu
		@Menu$[0], -,
		@Menu$[1], -,
		@Menu$[2], -,
		@Menu$[3], -,
		@Menu$[4], -,
		@Menu$[5], -,
		@Menu$[6], -,
		@Menu$[7], -,
		@Menu$[8], -,
		@Menu$[9], -,
		@Menu$[10], -;

	if (@menu > @i) close;
	
	set @req, @menu;
	set @rec, 0;
	
L_Item_Loop:
	if (@rec == @req || Mobpt < tvis) goto L_Item_Done;
	
	callsub SUB_give_item;
	set @rec, @rec + 1;
	goto L_Item_Loop;
	
L_Item_Done:
	if (@rec < @req) mes "Es sieht so aus, als wären deine Monsterpunkte ins Unzählbare gewachsen...";
	close;

L_Register:
	mes "[Ishi die Geschenkverteilerin]";
	mes "\"Hey, es sieht aus, als hättest du dich noch nicht registriert! Du kannst dich bei Aidan anmelden.\"";
	close;

L_NotEnough:
	mes "[Ishi die Geschenkverteilerin]";
	mes "\"Du hast nicht genug Monsterpunkte für ein Geschenk. Du musst erst einige Monster töten.\"";
	close;


SUB_prep_menu:
	set @pts, Mobpt;
	set @dif, tvis;
	
L_Menu_Loop:
	if (@pts < @dif || @i == 10) goto L_Menu_Done;
	
	set @Menu$[@i], @i + 1;
	
	set @i, @i + 1;
	set @pts, @pts - @dif;
	set @dif, @dif + 1;
	
	goto L_Menu_Loop;
L_Menu_Done:
	set @Menu$[@i], "Nein, danke";
	return;

SUB_give_item:
	set Mobpt, Mobpt - tvis;
	set tvis, tvis + 1;
	set @item$, @items$[rand(getarraysize(@items$))];
	
	getitem @item$, 1;
	
	mes "Du erhälst 1 " + getitemname(@item$) + "!";
	
	return;
}

