
001-1,48,52,0	script	[Aidan]	NPC102,{
	if(MPQUEST == 0) goto Register;
	mes "[Aidan der Monsterkenner]";
	mes "Du hast momentan " +Mobpt+ " Monsterpunkte. Diese Punkte verdienst du dir, wenn du Monster tötest.";
	close;
Register:
	mes "[Aidan der Monsterkenner]";
	mes "Oh, es scheint, als wenn du noch nicht bei der Monstersuche registriert bist. Möchtest du dich registrieren?";
	next;
Choice:
	menu 
		"Ja, ich möchte mich registrieren.",L_R,
		"Nein, danke, jetzt gerade nicht.",L_N,
		"Kannst du mir noch ein paar Informationen darüber geben?",L_I;
L_R:
	mes "[Aidan der Monsterkenner]";
	mes "Gib mir eine Sekunde, um deine Personalien durchzuprüfen.";
	next;
	mes "[Aidan der Monsterkenner]";
	mes "Gut, du bist nun bei uns registriert!";
	mes "Herzlich Willkommen in der Quest-Welt!";
	set MPQUEST,1;
	close;
L_N:
	mes "[Aidan der Monsterkenner]";
	mes "Hmm, OK, du weißt eigentlich gar nicht, worum es hier geht. Du kannst dir auch zuerst Informationen darüber holen.";
	close;
L_I:
	mes "[Aidan der Monsterkenner]";
	mes "Hier in The Mana World, gibt es Belohnungen für das Besiegen von diversen Monstern.";
	mes "Zum Beispiel gibt es Monsterpunkte: Jedes Monster, dass du tötest, hat eine gewisse Anzahl an Monsterpunkten, welche auf dein Konto gutgeschrieben werden.";
	mes "Je mehr Punkte du hast, desto mehr teure Sachen bekommst du, wenn du deine Monsterpunkte eintauschst.";
	next;
	mes "[Aidan der Monsterkenner]";
	mes "Und was meinst du, willst du dich bei uns registrieren?";
	next;
	goto Choice;
}

