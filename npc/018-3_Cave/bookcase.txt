
018-3,75,123,0	script	#DemonMineBookcase1	NPC127,{
	if (QUEST_demon_mines == 0) goto L_Search;

	mes "You see nothing different about the bookcase.";
	mes "Do you want to search it again anyways?";
	next;
	menu
		"Ja", L_Search,
		"Nein", -;
	close;

L_Search:
	mes "Nach dem du ein Buch gefunden hast suchst du eine stelle die du entziffern kannst.";
	next;

	mes "Eine der Inschriften sagt aus über eine Bariere vor dem Raum der Truhe.";
	mes "";
	mes "Um hinein zu kommen brauchst du die sehle eines JackO's.";
	next;

	mes "EIne andere Seite... hier steht das man so einiges brauch um die Truhe zu öffnen.";
	mes "";
	mes "Es sieht so aus als würden ne mänge Gegenstände gebraucht werden.";
	next;

	mes "Der Schleim einer Made.";
	mes "Ein Pilz, groß oder klein.";
	mes "Die Blüte einer Pflanze.";
	next;

	mes "Eine Perle von der See.";
	mes "Eine Spitze; je härter, desto besser.";
	mes "Ein Stück vom Wald. Ein Holz sollte genügen.";
	next;

	mes "Eine Antenne. am besten in Pink.";
	mes "Die Zunge von vier verschiedenen Schlangen.";
	mes "Ein Schlüssel zum öffnen einer Truhe.";
	next;

	mes "Ein Haufen Asche.";
	mes "Vier verschiedenfarbige Blätter.";
	mes "Ein Beinchen.";
	next;

	mes "Eine Lampe für die dunkelheit.";
	mes "Stein und Eisen in einem.";
	mes "Drei verschiedene Stinger.";
	next;

	mes "Ein Kocon von einem Seidenwurm.";
	mes "Die Sehle eines Monsters.";
	mes "Ein kleines stück dunkelheit.";
	next;

	mes "Und eine Flasche mit Wasser.";
	mes "";
	mes "Du fängst an dich zu wundern ob du die Liste richtig gelesen hast.";
	next;

	if (QUEST_demon_mines == 0) set QUEST_demon_mines, 1;
	close;
}
