
019-1,77,44,0	script	Snowman	NPC129,{
	if( ChristmasQuest == 1) goto L_Done;

	mes "[Schneemann]";
	mes "\"Hallo du da, junge Person.";
	mes "Willst du einen Weihnachtshut haben?\"";
	next;

L_Menu:
	menu
		"Ja", L_Sure,
		"Nein", -,
		"Was brauchst du dafuer?", L_Need;
	mes "[Schneemann]";
	mes "\"Naja zu schade, aber trotzdem hoffe ich das der Weihnachtsgeist in dir wohnt!\"";
	close;

L_Sure:
	mes "[Schneemann]";
	mes "\"Hmm, zeig mal was du so hast.\"";
	next;
	if(countitem("Candy") < 15) goto L_NoItem;
	if(countitem("ChocolateBar") < 10) goto L_NoItem;
	if(countitem("CactusPotion") < 5) goto L_NoItem;
	getinventorylist;
	if (@inventorylist_count > 99) goto L_TooMany;
	delitem "Candy", 15;
	delitem "ChocolateBar", 10;
	delitem "CactusPotion", 5;
	mes "[Schneemann]";
	mes "\"Hier hast du ihn, geniess ihn, den neuen Hut!\"";
	getitem "SantaHat", 1;
	set ChristmasQuest, 1;
	close;

L_Need:
	mes "[Schneemann]";
	mes "\"Fuer diesen speziellen Hut, brauche ich nur ein bisschen Magie und etwas Hilfe.";
	mes "War nur ein Scherz. Ich will dafuer etwas zu essen haben:";
	mes "15 [Bonbon]s";
	mes "10 [Schokoladen Tafel]n";
	mes "5 kleine Flaschen [Grosser Cactus Trank]\"";
	next;
	goto L_Menu;

L_NoItem:
	mes "[Schneemann]";
	mes "\"Naja, ich bin eigentlich an Essem interessiert und du hast wohl nicht genug davon.\"";
	close;

L_Done:
	mes "[Schneemann]";
	mes "\"Danke fuer deine Hilfe!\"";
	close;

L_TooMany:
	mes "[Schneemann]";
	mes "\"Es sieht aus als ob du nicht genug Platz dafuer haettest. Komm spaeter wieder, wenn du etwas Platz gemacht hast.\"";
	close;
}
