//

111-4.gat,22,61,0	script	[Schatz]	111,{
	if( CharlysCaveChest & 1) goto L_Finished;

	mes "[Schatz]";
	mes "Möchtest du sie öffnen?";
	next;
	menu
		"Ja", L_Ja,
		"Nein", -;
	close;

L_Ja:
	getinventorylist;
	if ((@inventorylist_count) > 95 - (countitem(2200) == 0) - (countitem(529) == 0)) goto L_TooMany;
	mes "[Schatz]";
	mes "Du hast sie geöffnet und findest verschiedene Sachen und eine Ratte, die sich in der Truhe versteckt hat!";
	getitem 3980, 1;
	getitem 4601, 1;
	getitem 600, 1;
	getitem 575, 1;
	getitem 645, 1;
	getitem 3916, 10;
	getitem 529, 2000;
	set zeny, zeny + 2000;
	getexp 5000,0;
	set CharlysCaveChest, CharlysCaveChest | 1;
	close;

L_Finished:
        mes "[Schatz]";
        mes "Du hast diesen Schatz schon einmal geöffnet.";
        close;

L_TooMany:
        mes "[Schatz]";
        mes "Du hast kein Platz in deinem Inventar für den Inhalt der Truhe. Du kannst später wieder kommen.";
	close;
}
