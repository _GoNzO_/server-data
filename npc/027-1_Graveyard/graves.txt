
function	script	GraveTooFar	{
	if (isin("027-1",89,54,122,76))
		return;
	mes "Du bist zu weit weg, du kannst absolut nix auf dem Grab erkennen.";
	close;
}

027-1,89,62,0	script	Grave1	NPC127,{
	callfunc "GraveTooFar";
	mes "~ DerLoisl ~";
	mes "Hat sich wohl fuer unseren Server Totgeackert.";
	close;
}

027-1,118,54,0	script	Grave2	NPC127,{
	callfunc "GraveTooFar";
	mes "~ GoNzO ~";
	mes "jak1's Bruder liegt hier vergraben, er war ein gute CoAdmin.";
	close;
}

027-1,111,62,0	script	Grave3	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Calimero ~";
	mes "Rest in Peace.";
	close;
}

027-1,112,76,0	script	Grave4	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Holly ~";
	mes "jak1's Beste Freundin.";
	mes "(Für Immer und Ewig!)";
	close;
}

027-1,120,76,0	script	Grave5	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Aroleon ~";
	mes "Mapped the swamp to the southeast.";
	close;
}

027-1,100,68,0	script	Grave6	NPC127,{
	callfunc "GraveTooFar";
	mes "~ John P ~";
	mes "Wrote the graveyard backstory and the dialog for the caretaker quest.";
	close;
}

027-1,94,70,0	script	Grave7	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Spit23 ~";
	mes "Made those freakin awesome cemetery gates.";
	close;
}

027-1,106,60,0	script	Grave8	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Black Don ~";
	mes "Contributed those cool gargoyle statues.";
	close;
}

027-1,92,54,0	script	Grave9	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Jaxad0127 ~";
	mes "Did all the scripting work for the graveyard. Made Caretaker's house.";
	close;
}

027-1,122,64,0	script	Grave10	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Ces Vargavind ~";
	mes "Scripted caretaker's daughter.";
	close;
}

027-1,102,62,0	script	Grave11	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Crush ~";
	mes "Organizer of the whole graveyard project, graveyard fence graphics, mapping of the graveyard itself.";
	close;
}

027-1,104,76,0	script	Grave12	NPC127,{
	callfunc "GraveTooFar";
	mes "~ Freeyorp ~";
	mes "Planned out monster stats and placements.";
	close;
}
