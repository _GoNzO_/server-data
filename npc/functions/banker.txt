
function	script	Banker	{
	if (BankAccount > 0) callsub S_MoveAccount;

L_Start:
	mes "[" + @npcname$ + "]";
	mes "\"Willkommen in der Bank!";
	mes "Wie kann ich dir helfen\"";
	next;
	menu	
		"Einzahlen", L_Dep,
		"Abheben", L_With,
		"Wie viel ist noch auf meinem Konto", L_Balance,
		"Auf wieder Sehen", L_Nev;
	
L_Dep:
	mes "[" + @npcname$ + "]";
	mes "\"Wie viel m�chtest du Einzahlen?\"";
	next;
       menu
		"Anderer Betrag", L_Dep_Input,
		"5,000 GP", L_Dep_5k,
		"10,000 GP", L_Dep_10k,
		"25,000 GP", L_Dep_25k,
		"50,000 GP", L_Dep_50k,
		"100,000 GP", L_Dep_100k,
		"250,000 GP", L_Dep_250k,
		"500,000 GP", L_Dep_500k,
		"1,000,000 GP", L_Dep_1kk,
		"Alles was ich besitze", L_Dep_All,
		"Ich hab meine Meinung Geaendert", L_Start,
		"Auf wieder Sehen", -;
	goto L_Nev;
	
L_Dep_Input:
	input @Amount;
	if (@Amount >= 0) goto L_Dep_Continue;
	mes "[" + @npcname$ + "]";
	mes "\"Ich brauche einen Positiven Betrag. Was m�chtest du jetzt tun?\"";
	menu	"Zur�ck zum Anfang", L_Start,
		"Noch ein mal", L_Dep_Input,
		"Alles Einzahlen", L_Dep_All,
		"Auf wieder Sehen", -;
	goto L_Nev;

L_Dep_5k:
	if (Zeny<5000) goto L_NoMoney;
	set @Amount, 5000;
	goto L_Dep_Continue;

L_Dep_10k:
	if (Zeny<10000) goto L_NoMoney;
	set @Amount, 10000;
	goto L_Dep_Continue;

L_Dep_25k:
	if (Zeny<25000) goto L_NoMoney;
	set @Amount, 25000;
	goto L_Dep_Continue;

L_Dep_50k:
	if (Zeny<50000) goto L_NoMoney;
	set @Amount, 50000;
	goto L_Dep_Continue;

L_Dep_100k:
	if (Zeny<100000) goto L_NoMoney;
	set @Amount, 100000;
	goto L_Dep_Continue;

L_Dep_250k:
	if (Zeny<250000) goto L_NoMoney;
	set @Amount, 250000;
	goto L_Dep_Continue;

L_Dep_500k:
	if (Zeny<500000) goto L_NoMoney;
	set @Amount, 500000;
	goto L_Dep_Continue;

L_Dep_1kk:
	if (Zeny<1000000) goto L_NoMoney;
	set @Amount, 1000000;
	goto L_Dep_Continue;

L_Dep_All:
	if (Zeny<1) goto L_NoMoney;
	set @Amount, Zeny;
	
L_Dep_Continue:
	if (Zeny < @Amount) goto L_NoMoney;
	set Zeny, Zeny - @Amount;
	set #BankAccount, #BankAccount + @Amount;
	goto L_Balance;
	
L_With:
	mes "[" + @npcname$ + "]";
	mes "\"Wie viel m�chtest du Abheben?\"";
	menu
		"Anderer Betrag", L_With_Input,
		"5,000 GP", L_With_5k,
		"10,000 GP", L_With_10k,
		"25,000 GP", L_With_25k,
		"50,000 GP", L_With_50k,
		"100,000 GP", L_With_100k,
		"250,000 GP", L_With_250k,
		"500,000 GP", L_With_500k,
		"1,000,000 GP", L_With_1kk,
		"Alles was auf meinem Konto ist", L_With_All,
		"Ich habe meine Meinung geaendern", L_Start,
		"Auf wieder Sehen", -;
	goto L_Nev;
	
L_With_Input:
	input @Amount;
	if (@Amount >= 0) goto L_With_Continue;
	mes "[" + @npcname$ + "]";
	mes "\"Ich brauche einen Positiven Betrag. Was m�chtest du jetzt tun?\"";
	menu	"Zurueck zum Anfang", L_Start,
		"Noch ein mal", L_With_Input,
		"Alles Abheben", L_With_All,
		"Auf wieder Sehen", -;
	goto L_Nev;

L_With_5k:
	if (#BankAccount < 5000) goto L_NoMoney;
	set @Amount, 5000;
	goto L_With_Continue;

L_With_10k:
	if (#BankAccount < 10000) goto L_NoMoney;
	set @Amount, 10000;
	goto L_With_Continue;

L_With_25k:
	if (#BankAccount < 25000) goto L_NoMoney;
	set @Amount, 25000;
	goto L_With_Continue;

L_With_50k:
	if (#BankAccount < 50000) goto L_NoMoney;
	set @Amount, 50000;
	goto L_With_Continue;

L_With_100k:
	if (#BankAccount < 100000) goto L_NoMoney;
	set @Amount, 100000;
	goto L_With_Continue;

L_With_250k:
	if (#BankAccount < 250000) goto L_NoMoney;
	set @Amount, 250000;
	goto L_With_Continue;

L_With_500k:
	if (#BankAccount < 500000) goto L_NoMoney;
	set @Amount, 500000;
	goto L_With_Continue;

L_With_1kk:
	if (#BankAccount < 1000000) goto L_NoMoney;
	set @Amount, 1000000;
	goto L_With_Continue;

L_With_All:
	if (#BankAccount < 0) goto L_NoMoney;
	set @Amount, #BankAccount;

L_With_Continue:
	if (#BankAccount < @Amount) goto L_NoMoney;
	set Zeny, Zeny + @Amount;
	set #BankAccount, #BankAccount - @Amount;
	goto L_Balance;
	
L_Balance:
	mes "[" + @npcname$ + "]";
	mes "\"Auf deinem BankKonto sind jetzt:";
	mes #BankAccount + " GP\"";
	goto L_Start;
	
L_Nev:
	mes "[" + @npcname$ + "]";
	mes "\"Auf Wieder Sehen.\"";
	return;
	
L_NoMoney:
	mes "[" + @npcname$ + "]";
	mes "\"Oh, ich sehe du hast nicht genug GP.\"";
	goto L_Start;
	
S_MoveAccount:
	set #BankAccount, #BankAccount + BankAccount;
	set BankAccount, 0;
	return;
}
