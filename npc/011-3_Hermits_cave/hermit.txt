
011-3,30,30,0	script	[Arkim]	NPC116,{
        mes "[Arkim the Hermit]";
        mes "\"Möchtest du wieder zurück nach draussen?\"";
        menu
        	"Ja bitte", L_Sure,
        	"Nicht jetzt", -,
		"... Warum lebst du hier?", L_Interested;
        close;

L_Sure:
        warp "011-1", 60, 95;
        close;

L_Interested:
	mes "[Arkim the Hermit]";
	mes "\"Oh, hab darüber nie nachgedacht!\"";
	next;
	mes "[Arkim the Hermit]";
	mes "\"Ich denke am meisten interessieren mich die Experimente mit den Fledermäusen.\"";

	menu
		"Ich sehs...", -,
		"Was für Experimente sind das?", L_Experiment;
	close;

L_Experiment:
	mes "[Arkim the Hermit]";
	mes "\"Das weiss ich selbst nicht so genau...\"";

	next;

	mes "Du schaust dir den alten Mann an, seine Augen öffnen sich weit, und es scheint so als ob er Energie getankt hat.";

	next;

	mes "[Arkim the Hermit]";
	mes "\"ICH DENK ICH MAG ES IHNEN DIE FLÜGEL ABZUSCHNEIDEN, UM ZU SEHEN WIE DIESE FUNKTIONIEREN!!  HAHAHA!\"";

	menu
		"DU BIST VERRÜCKT!", -,
		"Richtig... ich auch!  Kann ich helfen?", L_Quest;
	close;

L_Quest:
	set @dq_level, 20;
	set @dq_cost, 4;
	set @dq_count, 3;
	set @dq_name$, "BatWing";
	set @dq_friendly_name$, "FlederMaus-Flügel";
	set @dq_money, 600;
	set @dq_exp, 100;

	callfunc "DailyQuest";

	next;	

	mes "[Arkim the Hermit]";
	mes "\"Zur Erinnerung: Leg deine Zahnbürste niemals in die Nähe eines Schleims!\"";
	close;
}
