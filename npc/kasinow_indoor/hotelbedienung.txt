kasinowin,115,78,0	script	[James]	NPC107,{
	mes "[James]";
	mes "Hallo,";
	mes "wie kann ich dir behilflich sein?";
	next;
	menu
		"Ich hätte gern ein Zimmer.",L_1,
		"Ich möchte mein Zimmer wieder frei geben.",L_2,
		"Ich würde gern ein Bier bestellen.",L_3,
		"Könnten Sie mir sagen wo das Kasino ist?",L_4,
		"Wie komme ich hier wieder raus?",L_5,
		"Alles bestens, Danke",-;
	close;
L_3:
	mes "tut mir leid, ich kann ihnen kein Bier geben,";
	mes "im 2. Stock gibt es ein Restaurant";
	close;
L_4:
	mes "Das Kasino ist im 1. Stock";
	close;
L_5:
	mes "wenn sie aus dem Gebäude wollen gehen sie in den Fahrstuhl rechts.";
	mes "E. wie Erdgeschoss";
	close;
L_1:
	if (hatzimmer >= 1) goto L_hat;
	mes "Du kannst hier leider nur Zimmer kaufen,";
	mes "Möchtest du eins Kaufen? das kostet dann 10000€";
	menu
		"Na klar",L_will,
		"So Teuer?!",-;
	mes "Das ist das einzige Hotel hier in der gegend...";
	mes "Komm jeder zeit wieder";
	close;
L_will:
	if (Zeny < 10000) goto L_No_Money;
	set Zeny, Zeny - 10000;
	set @temp,rand(1,14);
	set hatzimmer, @temp;
	mes "Hier ist der Schlüssel, Zimmer Nummer " + @temp + ".";
	if (@temp == 1 || @temp == 2 || @temp == 3 || @temp == 4 || @temp == 5 || @temp == 6 || @temp == 7) set @lor$,"linken";
	if (@temp == 8 || @temp == 9 || @temp == 10 || @temp == 11 || @temp == 12 || @temp == 13 || @temp == 14) set @lor$,"rechten";
	if (@temp < 8) set @unter,1;
	if (@temp > 7) set @ueber,1;
	if (@unter == 1) set @wvlte, @temp;
	set @xyz, @temp - 7;
	if (@ueber == 1) set @wvlte, @xyz;
	next;
	mes "Das Zimmer ist das " + @wvlte + "'te auf der " + @lor$ + " Seite, von Oben!"; 
	close;

L_2:
	mes "Sicher das du dein Zimmer abgeben möchtest? du musst beim nächsten mal wieder 10000€ Zahlen";
	menu 
		"Ganz Sicher!",L_ok,
		"Nein, will ich nicht!",-;
	close;
L_ok:
	mes "Die Schlüssel Bitte";
	set hatzimmer,0;
	close;
L_hat:
	mes "Du hast schon ein Zimmer, für was bsolltest du ein 2. gebrauchen?";
	close;

L_No_Money:
	mes "Du hast nicht genügend geld um dir ein Zimmer zu Kaufen!";
	close;
}
