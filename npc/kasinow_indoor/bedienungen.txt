kasinowin,34,24,0	shop	[Bedienung1]	NPC140,539:-1,513:-1,676:-1
kasinowin,24,21,0	shop	[Bedienung2]	NPC140,539:-1,513:-1,676:-1
kasinowin,19,25,0	shop	[Bedienung3]	NPC140,539:-1,513:-1,676:-1

kasinowin,32,53,0	script	[Bandit1]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,34,53,0	script	[Bandit2]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,36,53,0	script	[Bandit3]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,38,53,0	script	[Bandit4]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,40,53,0	script	[Bandit5]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,32,59,0	script	[Bandit6]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,34,59,0	script	[Bandit7]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,36,59,0	script	[Bandit8]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,38,59,0	script	[Bandit9]	NPC127,{
	callfunc "SlotMachine"; close; }
kasinowin,40,59,0	script	[Bandit10]	NPC127,{
	callfunc "SlotMachine"; close; }

kasinowin,28,45,0	shop	[MoneyChanger]	NPC124,503:10

kasinowin,28,55,0	script	[BlackJack]	NPC107,{
	mes "[Croupier]";
	mes "\"Würdest du gerne Black Jack spielen?";
	mes "Dafür benötigst du 15 Casinomünzen.\"";
	next;
	
	menu
		"Ja", L_Begin,
		"Nein", -;
	mes "\"Wie du wünschst.\"";
	close;

L_Begin:
	if(countitem("CasinoCoins") < 15) goto L_NoCoin;
	delitem "CasinoCoins", 15;
	set @croupier, rand(0, 4);
	set @croupier, @croupier + 17;
	set @player, rand(4, 21);
	mes "\"Du hast " + @player + " mit deinen Karten.";
	if(@player == 21) goto L_End;
	mes "Möchtest du eine weitere Karte?\"";
	next;

	menu
		"Ja", L_Another,
		"Nein", L_End;
	
L_Another:
	set @tempace, rand(2, 11);
	if (@tempace == 11) goto L_Ace;
	set @player, @player + @tempace;
	if (@player > 21) goto L_Lost;
	if (@player == 21) goto L_End;
	mes "\"Du hast " + @player + " mit deinen Karten.";
	mes "Möchtest du eine weitere Karte?\"";
	next;

	menu
		"Ja", L_Another,
		"Nein", L_End;
L_End:
	if (@player <= @croupier) goto L_Lost;
	mes "\"Gratulation, du hast gewonnen!";
	mes "Ich habe " + @croupier + ".";
	mes "Du erhältst 45 Casinomünzen.\"";
	getitem "CasinoCoins", 45;
	close;

L_NoCoin:
	mes "\"Du benötigst wenigstens 15 Casinomünzen.\"";
	close;

L_Lost:
	mes "\"Es tut mir leid, aber du hast verloren.";
	mes "Du hast " + @player + " mit deinen Karten.";	
	mes "Ich habe " + @croupier + ".\"";
	close;

L_Ace:
	set @player, @player + 11;
	if (@player > 21) set @player, @player - 10;
	if (@player > 21) goto L_Lost;	
	if (@player == 21) goto L_End;
	mes "Du hast " + @player + " mit deinen Karten.";	
	mes "Möchtest du eine weitere Karte?";
	next;

	menu
		"Ja", L_Another,
		"Nein", L_End;
	close;
}

kasinowin,19,54,0	script	[Roulette]	NPC107,{
	mes "\"Guten Abend, Monsieur...";
	mes "Wieviel möchten Sie setzen?\"";
	next;
	
	menu
		"1 Münze", L_b1,
		"5 Münzen", L_b5,
		"10 Münzen", L_b10,
		"50 Münzen", L_b50,
		"100 Münzen", L_b100,
		"Vielleicht werde ich später spielen", -;
	mes "Kommen Sie wieder.";
	close;

L_b1:
	set @bet, 1;
	goto L_Check;

L_b5:
	set @bet, 5;
	goto L_Check;

L_b10:
	set @bet, 10;
	goto L_Check;

L_b50:
	set @bet, 50;
	goto L_Check;

L_b100:
	set @bet, 100;
	goto L_Check;
	
L_Check:
	if(countitem("CasinoCoins") < @bet) goto L_NoCoin;
	delitem "CasinoCoins", @bet;
	menu
		"Wählen Sie eine Farbe", -,
		"Wählen Sie eine Zahl", L_Number;
	menu
		"Schwarz", -,
		"Rot", -;
    @color = rand(2);
	if(@color == 1) goto L_Lost;
	mes "Sie haben gewonnen!";
	getitem "CasinoCoins", @bet * 2;
	close;

L_Number:
	menu
		"0", -, "00", -, "1", -, "2", -, "3", -, "4", -, "5", -, "6", -, "7", -, "8", -,
		"9", -, "10", -, "11", -, "12", -, "13", -, "14", -, "15", -, "16", -, "17", -, "18", -,
		"19", -, "20", -, "21", -, "22", -, "23", -, "24", -, "25", -, "26", -, "27", -, "28", -,
		"29", -, "30", -, "31", -, "32", -, "33", -, "34", -, "35", -, "36", -;

	if (@menu == 1) set @number, 0;
	if (@menu == 2) set @number, 37;
	if (@menu >= 3) set @number, @menu - 2;
	
	set @roulette, rand(38);
	if (@roulette == 37) mes "Die Kugel blieb auf der 00";
	if (@roulette < 37) mes "Die Kugel blieb auf der " + @roulette;
	if (@number != @roulette) goto L_Lost;
	mes "\"Sie haben gewonnen!\"";
	getitem "CasinoCoins", @bet * 10;
	close;

L_NoCoin:
	mes "\"Sie haben nicht genug Münzen.\"";
	close;

L_Lost:
	mes "\"Es tut mir leid, sie haben verloren.\"";
	close;
}
