
011-4,0,0,0,0	monster	CaveSnake	1021,40,0,0,Mob17::OnCaveSnake
011-4,0,0,0,0	monster	SpikyMushroom	1019,15,0,0,Mob17::OnSpikyMushroom

011-4,0,0,0	script	Mob17	NPCMINUS1,{
OnCaveSnake:
	set @mobId, 1021;
	callfunc "MobPoints";
	end;

OnSpikyMushroom:
	set @mobId, 1019;
	callfunc "MobPoints";
	end;
}
