013-1,111,68,0	script	[Rudolph]	NPC209,{
	if (weihnachten2010 == 9 && pixie == 1) goto rudolph_y;
	if (weihnachten2010 == 4) goto rudolph_x;
	mes "[Rudolph]";
	mes "...";
	close;

rudolph_x:
	mes "[Rudolph]";
	mes "\"Bitte befrei mich!\"";
	set weihnachten2010, 5;
	close;
rudolph_y:
	mes "[Rudolph]";
	mes "\"Ich danke dir...";
	mes "Geh doch bitte zum Weihnachtsmann und sag ihm das ich später komme, muss noch etwas erledigen\"";
	set weihnachten2010, 10;
	close;
}
