
008-1,36,26,0	script	[Andra]	NPC201,{
        mes "[Andra]";
	mes "\"Hallo, mein name ist Andra. Wie heist du?\"";
	next;
	input @name$;

        mes "[Andra]";
	mes "\"Hello, " + @name$ + "! Was ist deine Lieblings-Zahl?\"";
	next;
	input @num;

	if (@num == 5) goto L_Same;

        mes "[Andra]";
	mes "\"Die mag ich nicht.\"";
	close;

L_Same:
        mes "[Andra]";
	mes "\"Meine auch!\"";
	close;
}
