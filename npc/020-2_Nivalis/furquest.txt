 
020-2,102,28,0	script	[Agostine]	NPC137,{
	if (QUEST_WG_state == 1) goto L_State_0_3;
	if (QUEST_WG_state == 2) goto L_State_1;
	if (QUEST_WG_state == 3) goto L_State_2;
	if (QUEST_WG_state == 4) goto L_State_4;
	if (QUEST_WG_state == 5) goto L_State_4_success;
	if (QUEST_WG_state == 6) goto L_State_6;
	if (QUEST_WG_state == 7) goto L_State_11;
	if (QUEST_WG_state == 8) goto L_State_12;
	if (QUEST_WG_state >= 9) goto L_State_13;
 
	mes "\"Etwas steigt, etwas faellt...\"";
	next;
	mes "\"Mmmm...?\"";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Oh, ein Kunde! Ich stelle mich kurz vor: Mein Name ist Agostine!";
	mes "Einige Leute sagen, ich sei der beste Kuerschner auf der Welt,";
	mes "aber ich denke ich bin der beste im ganzen Universum!";
	mes "So mein Freund, was kann ich fuer dich tun?\"";
	next;
	menu
		"Ich will was neues fuer meine Garderobe!", L_State_0_1,
		"Oh nein, nichts danke!", -;
	close;


L_State_0_1:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"naja weisst du... Die harte Arbeit macht mich muede!";
	mes "Kannst du mir was erfrischendes zu trinken geben?\"";
	next;
	menu
		"Sicher, was fuer Getraenke magst du so?", L_State_0_2,
		"Ich bin nicht dein Kellner!", -;
	close;

L_State_0_2:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Wir haben hier leider keine Bar. Gib mir einfach verschiedene Getraenke zum probieren.";
	mes "Ich will irgendwas neues trinken.\"";
	next;
	menu
		"Ich werde dir einige Getraenke zum probieren bringen!", L_State_Accept,
		"Villeicht macht bald eine Bar auf. Warte bis dahin darauf.", -;
	close;

L_State_Accept:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Vielen Dank.\"";
	set QUEST_WG_state, 1;
	close;

L_State_0_3:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"So was hast du mir denn mitgebracht?\"";
	next;
	menu
		"Einen Kaktus Trank.", L_State_0_4,
		"Einen grossen Kaktus Trank.", L_State_0_5,
		"Etwas Milch.", L_State_0_6,
		"Ein Glas Bier.", L_State_0_7,
		"Einen Eisentrank.", L_State_0_9,
		"Einen Konzentrationstrank.", L_State_0_8,
		"Im Moment noch nichts.", -;
	close;

L_State_0_4:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, einen Kaktus Trank?\"";
	if (countitem("CactusDrink") < 1) goto L_State_neg;
	next;
	delitem "CactusDrink", 1;
	goto L_State_bad;
	close;

L_State_0_5:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, einen grossen Kaktus Trank?\"";
	if (countitem("CactusPotion") < 1) goto L_State_neg;
	next;
	delitem "CactusPotion", 1;
	goto L_State_bad;
	close;

L_State_0_6:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, eine Tuete Milch?\"";
	if (countitem("Milk") < 1) goto L_State_neg;
	delitem "Milk", 1;
	goto L_State_bad;
	close;

L_State_0_7:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, ein Glas Bier?\"";
	if (countitem("Beer") < 1) goto L_State_neg;
	next;
	delitem "Beer", 1;
	goto L_State_bad;
	close;

L_State_0_8:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, einen Konzentrazionstrank?\"";
	if (countitem("ConcentrationPotion") < 1) goto L_State_neg;
	next;
	delitem "ConcentrationPotion", 1;
	goto L_State_bad;
	close;

L_State_0_9:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Soso, Einen Eisentrank?\"";
	if (countitem("IronPotion") < 1) goto L_State_neg;
	next;
	delitem "IronPotion", 1;
	mes "Agostine sieht erfreut aus und trinkt den Trank.";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Oh! Das schmeckt so gut! Ich fuehle mich wie ein neuer Mann, mein Freund!\"";
	next;
	set Zeny, Zeny + 500;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Das ist ein kleines Geschenk fuer deine Hilfe. Nimm die 500 GP mein Freund!\"";
	set QUEST_WG_state, 2;
	close;

L_State_neg:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Du hast wohl gelogen. Du hast nichtmal das Getraenk von dem du erzaehlt hast.\"";
	close;

L_State_bad:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Naja also das schmeckt nicht besonders. Bring mir irgendetwas anderes.\"";
	close;


L_State_1:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Mmmm mal sehen... Dieses Jahr sind helle Kleider der Hit!";
	mes "Willst du ein paar Schneeweisse Handschuhe haben?\"";
	next;
	menu
		"Nein, danke. Ich mag nur dunkle Klamotten...", -,
		"Kannst du das wirklich fuer mich tun?", L_State_2;
	close;

L_State_2:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Nun gut, die Winterhandschuhe sind vorallem wegen ihrem weichen Fell so modisch!";
	mes "Nur die besten Felle koennen fuer ein paar Handschuhe gebraucht werden!";
	mes "Aber wie du siehst, bin ich ein Kuerschner und kein Jaeger! Koenntest du mir also das beste Fell geben was du siehst?\"";
	set QUEST_WG_state, 3;
	next;
	menu
		"Du bist verrueckt! Fuer sowas werde ich keine Tiere toeten!", -,
		"Sicher, ich werde bald zurueck sein!", L_State_3;
	close;

L_State_3:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Vergiss nicht, ich will nur das beste Fell!\"";
	set QUEST_WG_state, 4;
	close;

L_State_4:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"So, mein Freund! Hast du mir gute Felle mitgebracht?\"";
	if (countitem("WhiteFur") < 1) close;
	next;
	menu
		"Hier, schau sie dir an!", L_State_4_try,
		"Ja, aber ich brauche sie fuer etwas anderes.", -;
	close;

L_State_4_try:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Zeig mal her mein Freund...! Die Felle muessen perfekt sein um Handschuhe daraus machen zu koennen!\"";
	mes "Agostine betrachtet das Fell genauer.";
	next;
	delitem "WhiteFur", 1;
	set @Temp1,rand(30);
	if (@Temp1 == 0) goto L_State_4_success;
	mes "Agostine wirft das Fell weg.";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Es wurde schrecklich geschnitten! Du solltest besser aufpassen wenn du Fluffys toetest!\"";
	if (countitem("WhiteFur") < 1) close;
	next;
	menu
		"Ich habe noch ein anderes Fell!", L_State_4_try,
		"Tut mir leid, ich werde vorsichtiger sein...", -;
	close;

L_State_4_success:
	mes "Agostine faengt an zu laecheln";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Das ist Perfekt! Wunderbar! Gute Arbeit mein Freund! Ich warden dein Fell verarbeiten!\"";
	next;
	set QUEST_WG_state, 5;
	goto L_State_5;

L_State_5:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"ehm das ist noch nicht alles mein Freund, meine Arbeit ist nicht kostenlos. Ich verlange 15.000 GP fuer diese Arbeit.\"";
	next;
	menu
		"Du bist kein Kuerschner, du bist ein Dieb!!!", -,
		"Ich denke das ist ein angemessener Preis...", L_State_5_pay;
	close;

L_State_5_pay:
	if (Zeny < 15000) goto L_State_5_nocash;
	getinventorylist;
	if (@inventorylist_count == 100) goto L_TooMany;
	set Zeny, Zeny - 15000;
	getitem "WinterGloves", 1;
	set QUEST_WG_state, 6;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Hier sind sie. Du wirst die modischsten Handschuhe der Welt  haben mein Freund!\"";
	close;

L_State_5_nocash:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Sieht aus als haettest du kein Geld mehr. Komm wieder wenn du das Geld hast.\"";
	close;

L_State_6:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Ah, ich erinnere mich an dich! Deine Handschuhe sehen sooo toll aus mein Freund!";
	mes "Ich habe uebrigens gute Neuigkeiten fuer dich. In Bezug auf ,,TMW Mode��";
	mes "Ich sah ein paar huebsche Fell Schuhe mein Freund.";
	mes "Ich kann die ein eigenes Paar machen wenn du willst!\"";
	next;
	menu
		"Das ist eine gute Idee!", L_State_10,
		"Ich bin schoen genug, danke", -;
	close;

L_State_10:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Perfekt! Um deiner Bitte nachzukommen brauche ich erneut, ein perfektes weisses Fell";
	mes "und ein paar normale Schuhe. Die kannst du uebrigens in Minen finden.";
	mes "Sie werden die so gut aussehen lassen!\"";
	set QUEST_WG_state, 7;
	close;

L_State_11:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"So mein Freund, hast du mir das richtige Fell mitgebracht?\"";
	if (countitem("WhiteFur") < 1) close;
	next;
	menu
		"Sicher, ich bin ein Fluffy Jaeger!", L_State_11_try,
		"Noch nicht, Sorry.", -;
	close;

L_State_11_try:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Zuerst will ich mir das Fell ansehen...\"";
	next;
	mes "Agostine betrachtet das Fell genauer.";
	next;
	delitem "WhiteFur", 1;
	set @Temp2,rand(30);
	if (@Temp2 == 0) goto L_State_11_success;
	mes "Agostine reisst das Fell auseinander.";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Dieses Fell war schrecklich. Damit werde ich nicht arbeiten!\"";
	if (countitem("WhiteFur") < 1) close;
	next;
	menu
		"Na gut, villeicht ist das hier besser!", L_State_11_try,
		"Ich werde andere Fluffys jagen...", -;
	close;

L_State_11_success:
	mes "Agostine faengt an zu laecheln";
	next;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Das ist gut genug mein Freund. Gute Arbeit.\"";
	set QUEST_WG_state, 8;
	goto L_State_12;

L_State_12:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Ich brauche aber auch noch ein paar normale Schuhe.";
	mes "Und natuerlich meine Bezahlung. 15.000 GP sind genug.\"";
	next;
	menu
		"In Ordnung, hier hast du alles.", L_State_12_pay,
		"Ich hab was vergessen. Ich komme nachher wieder!", -;
	close;

L_State_12_pay:
	if (Zeny < 15000) goto L_State_12_missing;
	if (countitem("Boots") < 1) goto L_State_12_missing;
	// No inventory check needed, as boots are removed, opening a slot
	set Zeny, Zeny - 15000;
	delitem "Boots", 1;
	getitem "FurBoots", 1;
	set QUEST_WG_state, 9;
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Geniess deine neuen Schuhe mein Freund. Sie machen dich wirklich schoen!\"";
	close;

L_State_12_missing:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Scheint so als haettest du was vergessen. Kontrolliere dein Inventar";
	mes "und deine Tasche!\"";
	close;

L_State_13:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Du bist so glanzvoll mein Freund.!";
	mes "Da habe ich wirklich gut gearbeitet!\"";
	close;

L_TooMany:
	mes "[Agostine, Der legendaere Kuerschner]";
	mes "\"Du hast keinen Platz mehr, wo du sie verstauen koenntest. Komm wieder wenn du etwas Platz gemacht hast.";
	close;
}

