
002-4,93,37,0	script	[Treasure]	NPC111,{
	if( ChestQuest & 1) goto L_Finished;

	mes "[Chest]";
	mes "Möchtest du sie öffnen?";
	next;
	menu
		"Ja", L_Yes,
		"Nein", -;
	close;

L_Yes:
	if(countitem("TreasureKey") < 3) goto L_Not_Enough;
	getinventorylist;
	if (@inventorylist_count == 100 && countitem("TreasureKey") > 3) goto L_TooMany;
	mes "[Truhe]";
	mes "Du hast sie geöffnet und findest ein [Kurzes Schwert]!";
	delitem "TreasureKey", 3;
	getitem "ShortSword", 1;
	set ChestQuest, ChestQuest | 1;
	close;

L_Not_Enough:
        mes "[Truhe]";
	mes "Es sieht so aus, als wäre es nicht der richtige Schlüssel...";
	close;

L_Finished:
        mes "[Truhe]";
        mes "Du hast diese Truhe schon einmal geöffnet.";
        close;

L_TooMany:
        mes "[Truhe]";
        mes "Du hast kein Platz in deinem Inventar für den Inhalt der Truhe. Du kannst später wieder kommen.";
	close;
}
