
009-3,0,0,0,0	monster	YellowSlime	1007,20,0,0,Mob35::OnYellowSlime
009-3,0,0,0,0	monster	RedSlime	1008,35,0,0,Mob35::OnRedSlime
009-3,0,0,0,0	monster	BlackScorpion	1009,25,0,0,Mob35::OnBlackScorpion

009-3,0,0,0	script	Mob35	NPCMINUS1,{
OnYellowSlime:
	set @mobId, 1007;
	callfunc "MobPoints";
	end;

OnRedSlime:
	set @mobId, 1008;
	callfunc "MobPoints";
	end;

OnBlackScorpion:
	set @mobId, 1009;
	callfunc "MobPoints";
	end;
}
